The wrappers folder contains "pass though" include files referencing commonly used 
include files in the Consultingwerk folder. Adding the Consultingwerk/Wrappers folder
to the propath for a compile time session can be used to provide a short-hand syntax
to the include files in the Consultingwerk folder:

{get-service.i ....} instead of {Consultingwerk/get-service.i ....}
