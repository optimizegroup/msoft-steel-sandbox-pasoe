﻿<?xml version="1.0" encoding="utf-8"?>
<styleLibrary office2007CustomBlendColor="" office2010ColorScheme="Blue" office2010CustomBlendColor="">
  <annotation>
    <lastModified>2019-02-16T18:48:42</lastModified>
  </annotation>
  <styleSets defaultStyleSet="Default">
    <styleSet name="Default" viewStyle="Office2010">
      <componentStyles>
        <componentStyle name="Inbox Button">
          <properties>
            <property name="BackColor" colorCategory="{Default}">220, 232, 245</property>
          </properties>
        </componentStyle>
        <componentStyle name="Inbox GroupBox">
          <properties>
            <property name="FlatStyle" colorCategory="{Default}">Flat</property>
          </properties>
        </componentStyle>
        <componentStyle name="Inbox Label">
          <properties>
            <property name="ForeColor" colorCategory="{Default}">30, 57, 91</property>
          </properties>
        </componentStyle>
        <componentStyle name="Inbox LinkLabel">
          <properties>
            <property name="LinkColor" colorCategory="{Default}">30, 57, 91</property>
          </properties>
        </componentStyle>
        <componentStyle name="Inbox MonthCalendar">
          <properties>
            <property name="BackColor" colorCategory="{Default}">207, 221, 238</property>
            <property name="ForeColor" colorCategory="{Default}">Black</property>
            <property name="ShowWeekNumbers" colorCategory="{Default}">True</property>
          </properties>
        </componentStyle>
        <componentStyle name="Inbox Panel">
          <properties>
            <property name="BackColor" colorCategory="{Default}">207, 221, 238</property>
            <property name="BorderStyle" colorCategory="{Default}">FixedSingle</property>
          </properties>
        </componentStyle>
        <componentStyle name="Inbox PropertyGrid">
          <properties>
            <property name="HelpBackColor" colorCategory="{Default}">White</property>
          </properties>
        </componentStyle>
        <componentStyle name="Inbox TextBox">
          <properties>
            <property name="BorderStyle" colorCategory="{Default}">FixedSingle</property>
          </properties>
        </componentStyle>
        <componentStyle name="Inbox TreeView">
          <properties>
            <property name="HotTracking" colorCategory="{Default}">True</property>
            <property name="ShowLines" colorCategory="{Default}">False</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraActivityIndicator" useFlatMode="True" />
        <componentStyle name="UltraCalculator" useFlatMode="True" />
        <componentStyle name="UltraCalendarCombo" viewStyle="Office2010" useFlatMode="True" />
        <componentStyle name="UltraCombo" viewStyle="Office2010" useFlatMode="True" />
        <componentStyle name="UltraDayView" viewStyle="Office2007" />
        <componentStyle name="UltraExplorerBar">
          <properties>
            <property name="ExpansionButtonCollapsedImage" colorCategory="{Default}">AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA6QAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAUIBgAAAIma9tgAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAABfSURBVBhXY3D18vsPwkDAAMORccn/41Oz/sMlvfyDwQrCY5P+x6Vk/k/NKoCo9PQL+u8fGvk/LDrhf2xS+v/kzHywQrhRoVHx/6MT0/4nZeSCJVAkQTgxLQdIwfj/GQB39VbY+xFBwgAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</property>
            <property name="ExpansionButtonExpandedImage" colorCategory="{Default}">AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA6wAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAUIBgAAAIma9tgAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAABhSURBVBhXTYrLDkAwFET9nAWLSkgTKh4tSps2FcTvD5ooi5O5M+dGAALSbHd8PRysn8ClwWz3u/4kKUpQ1qHmEmKxUO7wD1GcEiQkR0YrPzRixqAc9HrCy4dHvLSjwqRXXOSeV98XySvxAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraFormManager">
          <properties>
            <property name="Style" colorCategory="{Default}">Office2010</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraGanttView" viewStyle="Office2010" useFlatMode="True" />
        <componentStyle name="UltraGrid">
          <radioButtonGlyphInfo>Office2007RadioButtonGlyphInfo</radioButtonGlyphInfo>
        </componentStyle>
        <componentStyle name="UltraGridCellProxy" useFlatMode="True" />
        <componentStyle name="UltraMessageBoxManager" useFlatMode="True" />
        <componentStyle name="UltraMonthViewMulti" viewStyle="Office2007" useFlatMode="True" />
        <componentStyle name="UltraMonthViewSingle" viewStyle="Office2007" useFlatMode="True" />
        <componentStyle name="UltraNavigationBar" buttonStyle="Office2010Button" useFlatMode="True" />
        <componentStyle name="UltraOptionSet" buttonStyle="Office2010Button" useFlatMode="True">
          <radioButtonGlyphInfo>Office2010RadioButtonGlyphInfo</radioButtonGlyphInfo>
        </componentStyle>
        <componentStyle name="UltraProgressBar" useFlatMode="True" />
        <componentStyle name="UltraTabControl" buttonStyle="Office2010Button" useFlatMode="True">
          <properties>
            <property name="UseHotTracking" colorCategory="{Default}">True</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraTabStripControl" buttonStyle="Office2010Button" useFlatMode="True">
          <properties>
            <property name="UseHotTracking" colorCategory="{Default}">True</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraTilePanel" useFlatMode="True" />
        <componentStyle name="UltraTimelineView" viewStyle="Office2007" />
        <componentStyle name="UltraToolbarsManager">
          <properties>
            <property name="Style" colorCategory="{Default}">Office2010</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraTrackBar" viewStyle="Office2010" useFlatMode="True" />
        <componentStyle name="UltraTree" useFlatMode="True">
          <properties>
            <property name="NodeConnectorStyle" colorCategory="{Default}">None</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraWeekView" viewStyle="Office2007" />
      </componentStyles>
      <styles>
        <style role="ActivityIndicator" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="132, 157, 189" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ActivityIndicator_MarqueeFill">
          <states>
            <state name="Normal" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="0, 1, 0, 1">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAIAEAAAKJUE5HDQoaCgAAAA1JSERSAAAACQAAAAoIBgAAAGYFdL4AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADq8AAA6vAXERQ+4AAACWSURBVChTY1h+Zd1/XHgZlGaoOdz8HxnXHm4BY2QxhvT9Wf9hOGN/NhCDaAgbJs4Qtz/5PwSn/I9Hw7H7kv7H7Uv+zwBiEMIM1hfc/1tfhGAbMPaAY+vzQHEgZpC4qf5f/BZ2DJIDYQaFZ2b/FZ5C8TNzNAwSM/vPIPta5z8KfqXzXw5IgzBMjEH0hdx/OH6JhsHicv8BIFjbnAIAP6UAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
            </state>
          </states>
        </style>
        <style role="Base">
          <states>
            <state name="Normal" themedElementAlpha="Transparent" />
          </states>
        </style>
        <style role="Button" buttonStyle="Office2010Button" />
        <style role="CalendarComboControlArea">
          <states>
            <state name="Normal" backColor="237, 245, 253" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="CalendarComboDateButton">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_CalendarComboDateButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_CalendarComboDateButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_CalendarComboDateButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="CalendarComboDateButtonArea">
          <states>
            <state name="Normal" backColor="204, 219, 237" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="CalendarComboDropDown" borderStyle="TwoColor">
          <states>
            <state name="Normal" backColor="204, 219, 237" borderColor="133, 158, 191" backColor2="187, 206, 230" backGradientStyle="None" borderColor3DBase="133, 158, 191" backHatchStyle="None" borderColor2="White" />
          </states>
        </style>
        <style role="CalendarComboDropDownButton" borderStyle="Solid" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" foreColor="110, 112, 114" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="HotTracked">
              <resources>
                <name>Default_CalendarComboDropDownButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_CalendarComboDropDownButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ComboDropDownButton">
          <states>
            <state name="HotTracked" borderAlpha="Opaque" />
            <state name="Pressed" borderAlpha="Opaque" />
          </states>
        </style>
        <style role="DayViewTimeSlotDescriptor">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="DesktopAlertPinButton">
          <states>
            <state name="Normal" backColor="Transparent" borderAlpha="Transparent" backColor2="Transparent" />
            <state name="HotTracked" backColor="255, 242, 195" backColor2="255, 223, 130" />
            <state name="Pressed" backColor="252, 154, 64" backColor2="255, 179, 89" />
          </states>
        </style>
        <style role="DockAreaPaneContentArea">
          <states>
            <state name="Normal" backColor="207, 221, 238" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="DockAreaSplitterBarHorizontal">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Vertical" imageBackgroundStretchMargins="0, 1, 0, 1" />
          </states>
        </style>
        <style role="DockAreaSplitterBarVertical">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Horizontal" />
          </states>
        </style>
        <style role="DockControlPane">
          <states>
            <state name="Normal" backColor="239, 246, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="DockManagerSplitterBar" borderStyle="Solid" />
        <style role="DockPaneCaption" borderStyle="Solid">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
            <state name="Active" foreColor="Black" />
          </states>
        </style>
        <style role="DockPaneCloseButton">
          <states>
            <state name="HotTracked" backColorAlpha="Opaque" borderAlpha="Opaque" />
            <state name="Pressed" backColorAlpha="Opaque" borderAlpha="Opaque" />
          </states>
        </style>
        <style role="DockPanePinButton">
          <states>
            <state name="HotTracked" borderColor="244, 209, 82" backColorAlpha="Opaque" borderAlpha="Opaque" borderColor2="White" />
            <state name="Pressed" borderColor="194, 119, 43" backColorAlpha="Opaque" borderAlpha="Opaque" borderColor2="Transparent" />
          </states>
        </style>
        <style role="DockSlidingGroupHeader" buttonStyle="Office2010Button" />
        <style role="DropDownEditorButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" borderAlpha="Transparent" />
            <state name="HotTracked">
              <resources>
                <name>Default_CalendarComboDropDownButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_CalendarComboDropDownButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="DropDownResizeHandle">
          <states>
            <state name="Normal" backColor="236, 245, 253" foreColor="188, 198, 209" borderColor="167, 171, 176" backColor2="212, 229, 246" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="EditorControl">
          <states>
            <state name="Normal" backColor="Transparent" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ExplorerBarControlArea">
          <states>
            <state name="Normal" backColor="White" borderColor="114, 142, 173" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ExplorerBarGroupHeader" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" backColor="196, 213, 232" foreColor="30, 57, 91" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked">
              <resources>
                <name>Default_ExplorerBarGroupHeader_HotTracked</name>
              </resources>
            </state>
            <state name="Active">
              <resources>
                <name>Default_ExplorerBarGroupHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ExplorerBarGroupItemAreaInner">
          <states>
            <state name="Normal" backColor="White" backColor2="241, 245, 249" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="ExplorerBarGroupItemAreaOuter" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="253, 254, 254" borderColor="132, 157, 189" backColor2="241, 245, 249" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="ExplorerBarItem">
          <states>
            <state name="Normal" foreColor="30, 57, 91" />
          </states>
        </style>
        <style role="ExplorerBarItemScrollButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_ExplorerBarGroupHeader_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_ExplorerBarGroupHeader_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ExplorerBarNavigationOverflowButtonArea">
          <states>
            <state name="Normal" backColor="196, 213, 232" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ExplorerBarNavigationOverflowQuickCustomizeButton">
          <states>
            <state name="HotTracked">
              <resources>
                <name>Default_ExplorerBarGroupHeader_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ExplorerBarNavigationPaneCollapsedGroupArea">
          <states>
            <state name="Normal" backColor="196, 213, 232" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked">
              <resources>
                <name>Default_ExplorerBarGroupHeader_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ExplorerBarNavigationPaneExpansionButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" backColor="Transparent" borderColor="Transparent" backColorAlpha="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="207, 221, 238" backColor2="224, 235, 249" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="ExplorerBarNavigationSplitter">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="133, 158, 191" backColor2="200, 217, 239" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="FilterProviderActionButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="GanttViewControlArea">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GanttViewVerticalSplitterBar">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="182, 186, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridAddNewBox" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="133, 158, 191" borderAlpha="Opaque" backGradientStyle="None" borderColor3DBase="Transparent" backHatchStyle="None" borderColor2="133, 158, 191" />
          </states>
        </style>
        <style role="GridAddNewBoxButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="GridBandHeader">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridCaption">
          <states>
            <state name="Normal" backColor="248, 251, 255" foreColor="54, 123, 171" borderColor="Transparent" textHAlign="Left" fontBold="False" backGradientStyle="None" borderColor3DBase="Transparent" backHatchStyle="None" borderColor2="Transparent" />
          </states>
        </style>
        <style role="GridCardArea" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="132, 157, 189" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridCardCaption">
          <states>
            <state name="Normal" backColor="231, 233, 236" backColor2="215, 217, 219" backGradientStyle="Vertical" />
            <state name="Selected" backColor="255, 231, 113" backColor2="255, 149, 9" />
          </states>
        </style>
        <style role="GridCell" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
            <state name="Selected" borderColor="0, 25, 51" />
            <state name="Active" borderColor="Black" borderColor3DBase="Black" borderColor2="Black" />
            <state name="FilterRow" backColor="Office2007.ApplicationMenuFooterToolGradientLight" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridCellProxy">
          <states>
            <state name="Normal" backColor="237, 245, 253" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="242, 247, 252" borderColor="171, 186, 208" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridColScrollRegionSplitBox">
          <states>
            <state name="Normal" backColor="238, 238, 239" borderColor="182, 186, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridColumnHeader">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="255, 223, 107" borderColor="232, 191, 58" backColor2="255, 252, 230" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="GridControlArea">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" borderColor3DBase="160, 176, 199" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridFilterClearButton">
          <states>
            <state name="Normal" borderColor="160, 176, 199" />
          </states>
        </style>
        <style role="GridGroupByBox" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupByBoxPrompt">
          <states>
            <state name="Normal" backColor="Transparent" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupByExpansionIndicator" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupByRow">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="133, 158, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupByRowConnector">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupHeader">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" borderAlpha="Transparent" backGradientStyle="None" borderColor3DBase="160, 176, 199" backHatchStyle="None" borderColor2="160, 176, 199" />
          </states>
        </style>
        <style role="GridHeader" borderStyle="Solid">
          <states>
            <state name="Normal" borderColor="133, 158, 191" />
          </states>
        </style>
        <style role="GridRow" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="218, 220, 221" backGradientStyle="None" backHatchStyle="None" />
            <state name="Selected" backColor="182, 202, 234" borderColor="Black" backGradientStyle="None" borderColor3DBase="Black" backHatchStyle="None" borderColor2="Black" />
          </states>
        </style>
        <style role="GridRowEditTemplatePanel">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridRowSelector" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" imageBackgroundStretchMargins="1, 1, 2, 1" borderColor2="White" />
          </states>
        </style>
        <style role="GridRowSelectorHeader">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridScrollRegionSplitBox" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="238, 238, 239" borderColor="182, 186, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridSpecialRowSeparator" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="133, 158, 191" backColor2="200, 217, 239" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="GridSummaryFooter" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridSummaryFooterCaption" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridSummaryValue">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GroupPaneSplitterBarVertical">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Horizontal" />
          </states>
        </style>
        <style role="ListViewColumnHeader" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ListViewItem">
          <states>
            <state name="Selected" backColor="169, 193, 222" foreColor="0, 25, 51" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="255, 220, 45" foreColor="0, 36, 73" fontUnderline="False" backColor2="255, 255, 181" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="MessageBoxButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
            <state name="Active">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="MessageBoxButtonArea">
          <states>
            <state name="Normal" backColor="White" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="MessageBoxFooter">
          <states>
            <state name="Normal" backColor="220, 232, 246" borderColor="132, 157, 189" backColor2="179, 196, 216" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="MessageBoxHeader">
          <states>
            <state name="Normal" foreColor="59, 59, 59" />
          </states>
        </style>
        <style role="MonthViewMultiControlArea">
          <states>
            <state name="Normal" backColor="192, 219, 255" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="MonthViewSingleControlArea" borderStyle="Solid">
          <states>
            <state name="Normal" borderColor="141, 174, 217" />
          </states>
        </style>
        <style role="NavigationBarActionButton">
          <states>
            <state name="HotTracked" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 220, 45" borderColor3DBase="255, 185, 45" borderColor2="255, 185, 45" />
            <state name="Pressed" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 220, 45" borderColor3DBase="255, 185, 45" borderColor2="255, 185, 45" />
          </states>
        </style>
        <style role="NavigationBarControlArea">
          <states>
            <state name="Normal" backColor="White" borderColor="139, 160, 188" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="NavigationBarLocationDropDownButton">
          <states>
            <state name="HotTracked" backColor="255, 220, 45" borderColor="255, 185, 45" backColor2="255, 255, 181" />
            <state name="Pressed" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 220, 45" borderColor2="255, 185, 45" />
          </states>
        </style>
        <style role="NavigationBarLocationTextButton">
          <states>
            <state name="HotTracked" backColor="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="Pressed" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 192, 67" borderColor2="255, 185, 45" />
          </states>
        </style>
        <style role="NavigationBarPreviousLocationsDropDownButton">
          <states>
            <state name="HotTracked" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 192, 67" borderColor3DBase="255, 185, 45" borderColor2="255, 185, 45" />
            <state name="Pressed" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 220, 45" borderColor3DBase="255, 185, 45" borderColor2="255, 185, 45" />
          </states>
        </style>
        <style role="ProgressBarFill">
          <states>
            <state name="Normal" backColor="21, 233, 30" borderColor="Transparent" imageBackgroundStyle="Stretched" backColorAlpha="Opaque" backGradientStyle="None" borderColor3DBase="Transparent" backHatchStyle="None" borderColor2="Transparent">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAoQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAFQAAABUIAwAAAJ7JVaQAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAACfUExURV6/Y6fUrlnAXxfbKCPXMhvfLh7pMB3rLGzAcHzDgxXoHkzHVXTBeWa/aTHTPhXpHj/PSh7jMx/mNE3HVWC/ZB7kNF6+Y2O/Zx/nNV2+Yl+/Y2K/ZnXBejLTP0DPS12+Y33DhF+/ZGe/aW3AcGK/Z3TBeme/ah3qLEDPSjLTPh7oMB/nNBzfLhjaKDHSPh/kMyTWMj/OSh7rLCDnNR/pMAyycPkAAACBSURBVChTbcEHEsIgEAXQb+9i771rSCDq/c/m7uDOMIT3UIpBK3Bg6HquZMPQDJwZep4bOTGsnQubC6ycI5sIxC2dPZsJlGPQ8YwF8jwfiAfZMWRZ1hdPsmWw1lYLkKZppQBa61oBjDFDMRVIkmQkFn8f1AMv8kUjcCdvtD3KUeoH9iwcNlRwP9sAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</image>
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAoQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAFQAAABUIAwAAAJ7JVaQAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAACfUExURV6/Y6fUrlnAXxfbKCPXMhvfLh7pMB3rLGzAcHzDgxXoHkzHVXTBeWa/aTHTPhXpHj/PSh7jMx/mNE3HVWC/ZB7kNF6+Y2O/Zx/nNV2+Yl+/Y2K/ZnXBejLTP0DPS12+Y33DhF+/ZGe/aW3AcGK/Z3TBeme/ah3qLEDPSjLTPh7oMB/nNBzfLhjaKDHSPh/kMyTWMj/OSh7rLCDnNR/pMAyycPkAAACBSURBVChTbcEHEsIgEAXQb+9i771rSCDq/c/m7uDOMIT3UIpBK3Bg6HquZMPQDJwZep4bOTGsnQubC6ycI5sIxC2dPZsJlGPQ8YwF8jwfiAfZMWRZ1hdPsmWw1lYLkKZppQBa61oBjDFDMRVIkmQkFn8f1AMv8kUjcCdvtD3KUeoH9iwcNlRwP9sAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="ScheduleAppointment">
          <states>
            <state name="Normal" backColor="224, 233, 246" foreColor="Black" borderColor="141, 174, 217" backColor2="179, 201, 230" backGradientStyle="Vertical" />
            <state name="Selected" backColor="219, 230, 245" borderColor="Black" backColor2="181, 202, 230" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="ScheduleCurrentDay">
          <states>
            <state name="Normal" borderColor="201, 92, 5" />
            <state name="Selected" borderColor="201, 92, 5" />
          </states>
        </style>
        <style role="ScheduleDay">
          <states>
            <state name="Selected" backColor="169, 193, 222" foreColor="Black" borderColor="169, 193, 222" backGradientStyle="None" backHatchStyle="None" />
            <state name="Active" backColor="169, 193, 222" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ScheduleNonWorkingDay">
          <states>
            <state name="Normal" backColor="239, 242, 247" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ScheduleTrailingDay">
          <states>
            <state name="Normal" borderAlpha="Transparent" />
          </states>
        </style>
        <style role="ScrollBarButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_ScrollBarButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_ScrollBarButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_ScrollBarButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ScrollBarIntersection">
          <states>
            <state name="Normal" backColor="238, 238, 239" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="SpinButton">
          <states>
            <state name="HotTracked">
              <resources>
                <name>Default_SpinButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_SpinButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="SpinButtonDownMaxValue">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAtgAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAYIBgAAAA8OhHYAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAAsSURBVBhXYwjPbvqPCzP8//8fqwKQOFgSXQFMDC4JwsgSIIwiiY7xOKjpPwBNvGzN+33a8gAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinButtonDownNextItem">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAsQAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAQIBgAAAELGJX0AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAAnSURBVBhXYwjPbvqPCzP8//8fqwKQOFgSXQFMDC4JwsgS////ZwAAgs9IDdAcXyoAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinButtonDownNextPage">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAtwAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAoIBgAAAHjMRA0AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAAtSURBVChTYwjPbvqPCzP8//8fqwKQOFgSXQFMDC4JwsgSIIwiiY4HmYP+MwAAeDm57730zSwAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinButtonUpMinValue">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAsgAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAYIBgAAAA8OhHYAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAAoSURBVBhXYwjPbvqPCzP8/48bo3BAqpH5KBIwjCKJLIGsAI+Dmv4DALP9bM1EDfMAAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinButtonUpPrevItem">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAArQAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAQIBgAAAELGJX0AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAAjSURBVBhXY/j//z8ch2c3ASkEH0UChlEkkSWQFWCVgOCm/wAoVUgNIRFCDwAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinButtonUpPrevPage">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAwwAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAoIBgAAAHjMRA0AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAA5SURBVChTY/j//z8ch2c3ASkEH0UChlEkkSWQFWCVgGG4sdgwCgekGpmPIgHDKJLIEsgKyHXQfwYAwza576JDaiUAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="StatusBarPanel">
          <states>
            <state name="Normal" backColor="220, 232, 246" borderColor="194, 212, 232" backColor2="179, 196, 216" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="TabClientAreaHorizontal">
          <states>
            <state name="Normal" backColor="239, 246, 253" borderColor="159, 178, 199" borderAlpha="Transparent" backColor2="216, 228, 242" backGradientStyle="Vertical" borderColor3DBase="159, 178, 199" />
          </states>
        </style>
        <style role="TabClientAreaVertical">
          <states>
            <state name="Normal" borderColor="159, 178, 199" borderAlpha="Transparent" />
          </states>
        </style>
        <style role="TabControlCloseButton">
          <states>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TabItemAreaHorizontalBottom">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemAreaHorizontalTop">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemAreaVerticalLeft">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemAreaVerticalRight">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemHorizontalBottom">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Selected">
              <resources>
                <name>Default_TabControlTabItemHorizontalBottom_Selected</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TabControlTabItemHorizontalBottom_HotTracked</name>
              </resources>
            </state>
            <state name="HotTrackSelected">
              <resources>
                <name>Default_TabControlTabItemHorizontalBottom_Selected</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TabItemHorizontalTop">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Selected">
              <resources>
                <name>Default_TabControlTabItemHorizontalTop_Selected</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TabControlTabItemHorizontalTop_HotTracked</name>
              </resources>
            </state>
            <state name="HotTrackSelected">
              <resources>
                <name>Default_TabControlTabItemHorizontalTop_Selected</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TabItemVerticalLeft">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Selected">
              <resources>
                <name>Default_TabControlTabItemVerticalLeft_Selected</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TabControlTabItemVerticalLeft_HotTracked</name>
              </resources>
            </state>
            <state name="HotTrackSelected">
              <resources>
                <name>Default_TabControlTabItemVerticalLeft_Selected</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TabItemVerticalRight">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Selected">
              <resources>
                <name>Default_TabControlTabItemVerticalRight_Selected</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TabControlTabItemVerticalRight_HotTracked</name>
              </resources>
            </state>
            <state name="HotTrackSelected">
              <resources>
                <name>Default_TabControlTabItemVerticalRight_Selected</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TileCloseButton">
          <states>
            <state name="HotTracked" backColor="255, 236, 160" backColor2="255, 246, 202">
              <resources>
                <name>Default_TileCloseButton_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TileHeader">
          <states>
            <state name="Normal" backColor="191, 210, 234" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TileStateChangeButtonLarge">
          <states>
            <state name="HotTracked" borderAlpha="Opaque">
              <resources>
                <name>Default_TileStateChangeButtonLarge_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TileStateChangeButtonNormal">
          <states>
            <state name="HotTracked" backColor="255, 227, 124" backColor2="255, 251, 226">
              <resources>
                <name>Default_TileStateChangeButtonNormal_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TimelineViewColumnHeaderDays">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TimelineViewColumnHeaderHours">
          <states>
            <state name="Normal" backColor="White" borderColor="141, 174, 217" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TimelineViewColumnHeaderMonths">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TimelineViewColumnHeaderPrimaryInterval">
          <states>
            <state name="Normal" backColor="White" foreColor="30, 57, 91" borderColor="141, 174, 217" fontBold="True" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TimelineViewColumnHeaderWeeks">
          <states>
            <state name="Normal" backColor="White" foreColor="30, 57, 91" backGradientStyle="None" borderColor3DBase="255, 223, 134" backHatchStyle="None">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TimelineViewColumnHeaderYears">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TimelineViewControlArea" borderStyle="Solid">
          <states>
            <state name="Normal" borderColor="141, 174, 217" />
          </states>
        </style>
        <style role="TimelineViewDateTimeIntervalLabel">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ToolTip">
          <states>
            <state name="Normal" backColor="White" borderColor="118, 118, 118" backColor2="201, 217, 239" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="TrackBarButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" foreColor="107, 111, 115">
              <resources>
                <name>Default_TrackBarButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TrackBarButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_TrackBarButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TrackBarMajorTickmarks">
          <states>
            <state name="Normal" backColor="138, 156, 184" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TrackBarMinorTickmarks">
          <states>
            <state name="Normal" backColor="138, 156, 184" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TrackBarThumbHorizontal" buttonStyle="Office2010Button">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_TrackBarThumbHorizontal_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TrackBarThumbHorizontal_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_TrackBarThumbHorizontal_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TrackBarThumbVertical">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_TrackBarThumbVertical_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TrackBarThumbVertical_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAGwEAAAKJUE5HDQoaCgAAAA1JSERSAAAADQAAAAkIBgAAAOl6pmoAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACRSURBVChTYzhUofOfEAYCBmTMcKhG//+nQ/F4MUgNqqYGw/9fz+X9/32jCisGyYEwSB1CU4vJ//9POonCYLVgTR3m//896iAag9QzHOq2+v/zdsP/vw/bcGKQPAiD1EJs6rf9/+Fc0f8/91uwYpAcCIPUgTRANE22///0QBpeDFID0wDRNN3pPyGMrOH///8MAB0yYQetKgLRAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
            </state>
          </states>
        </style>
        <style role="TrackBarTrackHorizontal">
          <states>
            <state name="Normal" backColor="138, 156, 184" imageBackgroundOrigin="Container" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TrackBarTrackVertical">
          <states>
            <state name="Normal" backColor="138, 156, 184" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TreeCell">
          <states>
            <state name="Normal" backColor="Transparent" foreColor="30, 57, 91" borderColor="132, 157, 189" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TreeColumnHeader">
          <states>
            <state name="Normal" backColor="239, 245, 251" backColor2="225, 236, 250" backGradientStyle="Vertical" borderColor3DBase="132, 157, 189" />
          </states>
        </style>
        <style role="TreeControlArea" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TreeNode">
          <states>
            <state name="Normal" backColor="Transparent" foreColor="30, 57, 91" backGradientStyle="None" backHatchStyle="None" />
            <state name="Selected" backColor="169, 193, 222" foreColor="30, 57, 91" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="169, 193, 222" foreColor="30, 57, 91" fontUnderline="False" backGradientStyle="None" backHatchStyle="None" />
            <state name="Active" backColor="169, 193, 222" foreColor="30, 57, 91" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraButtonBase">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraCalculator">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraCalculatorButtonAction" buttonStyle="Flat">
          <states>
            <state name="Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonBase" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonImmediateAction">
          <states>
            <state name="Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonNumeric">
          <states>
            <state name="Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonOperator">
          <states>
            <state name="Normal" foreColor="30, 57, 91">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonPendingCalculations">
          <states>
            <state name="Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorDropDown">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraCalculatorDropDownHostCancelButton">
          <states>
            <state name="Normal" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorDropDownHostOkButton">
          <states>
            <state name="Normal" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraColorPicker">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraComboEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraComboEditPortion">
          <states>
            <state name="Normal" backColor="237, 245, 253" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraCurrencyEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraDateTimeEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraFontNameEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" borderAlpha="Opaque" />
          </states>
        </style>
        <style role="UltraFormattedTextEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraGroupBoxContentArea">
          <states>
            <state name="Normal" backColor="White" borderColor="133, 158, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraGroupBoxExpansionIndicator">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraMaskedEdit">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraNumericEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraPictureBox">
          <states>
            <state name="Normal" borderColor="Transparent" borderColor3DBase="Transparent" borderColor2="Transparent" />
          </states>
        </style>
        <style role="UltraProgressBar" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" borderColor2="Transparent" />
          </states>
        </style>
        <style role="UltraSplitterButton">
          <states>
            <state name="Normal" backColor="Transparent" borderColor="Gray" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked">
              <resources>
                <name>Default_CalendarComboDropDownButton_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraSplitterHorizontal" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="UltraSplitterVertical">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Horizontal" />
          </states>
        </style>
        <style role="UltraTabControl">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" borderColor3DBase="159, 178, 199" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraTabStripControl">
          <states>
            <state name="Normal" borderColor3DBase="159, 178, 199" />
          </states>
        </style>
        <style role="UltraTextEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraTile" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="144, 154, 166" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraTimeSpanEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraTimeZoneEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ValueList">
          <states>
            <state name="Normal" borderColor="167, 171, 176" />
          </states>
        </style>
      </styles>
    </styleSet>
    <styleSet name="WinKit">
      <styles>
        <style role="UltraPanel">
          <states>
            <state name="Normal" backColor="224, 233, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
      </styles>
    </styleSet>
    <styleSet name="error" basedOn="Default">
      <styles>
        <style role="EditorControl">
          <states>
            <state name="Normal" backColor="255, 158, 158" imageHAlign="Right" fontBold="True" backGradientStyle="None" backHatchStyle="None">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAGwMAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABAIBgAAAB/z/2EAAAAEZ0FNQQAAsY8L/GEFAAAACXBIWXMAAAsMAAALDAE/QCLIAAAAB3RJTUUH2AQPDQ01dJ+rSQAAAqpJREFUOE+1kXtIU1Ecx6+G9k9bam8f06WQiJhJoGJWaIllIwoqJBCRTAizaG6alppzm1vbGpbTTSczSqg/MqOXVtgDRJIypNRELXHOOedjc25ubn472Qiiogj6wOF+z72/z++cew713xBdYJ7RXI2ea6zaZqk8v5njfv13iEqYsc3Xk1zd7cfQ23ECzZqEJXl52A735z/iKbvE6Op6cgD6gWyYtFwYBtiQXGR2px+nVrhrfo+0NCT7UdMujPdlYMnRiSX7LUwNc/GyJRnCgtBcd9mvKc5l+NUIwicn+jNgm24EoCRDAavxCmaGMqGo2DItKPRf5y7/GX5hUM2re4nQfThMxBdQKRUQiSpJvobx91F41xYEHjtE7S7/EX5+cMxtZYJzou8IbLNqIqmRuu8QPDxpgJOPeUM69G+9cLOK6eLlM2Ld2nc8REUBHa8fJ0HXexBYbCVSCRISoxG9NZLkcmBBismBcIy+WQMBm9lFHM9vKoFfGJx5V5NKtp4Gu7kOmJcRiQMhnwUWK45kAWATwTHHxVT/SrTdYIB3NvDksnwui+YjLw3Tdz6IJyfPAuz3AYuESMV4/rQMrQ/LSL4MWElTey3MYykwDa2GmMMwcnLoflTBKX95+52vq+8hV9ZCZCkprgIcKnh7bQRFeZMGDeQXqkmDOnK11TCN+KLn2Saws/xrKEVFxPxoTzJZfS9cZiGcZjEWzTK4LHKMDpZj5KMALquSzOvhtGhIboLFeBROgw9kBQF2SlIUOqntSYP5cxRmBldhdogO0zAdc5/osGlpsI3RYCVPq46OhXE67Ho6Fo2+cBjWg3c6YILi5mxIEXNCtI3y3WiQkiHZCbUkAWpxPOrFcVBVbkedMAZKQTRU/CjU8iJRy48ALy9Ql5exdv/yQf47FPUFlpO1IUsKh9UAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</image>
            </state>
          </states>
        </style>
      </styles>
    </styleSet>
    <styleSet name="UltraGridSearchControl">
      <styles>
        <style role="UltraPanel">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraTextEditor">
          <states>
            <state name="Normal" fontBold="True" fontSize="20" />
          </states>
        </style>
        <style role="UltraNumericEditor">
          <states>
            <state name="Normal" fontBold="True" fontSize="20" />
          </states>
        </style>
      </styles>
    </styleSet>
  </styleSets>
  <resources>
    <resource name="Default_CalendarComboDateButton_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAUwMAAAKJUE5HDQoaCgAAAA1JSERSAAAALwAAABgIBgAAAGpYpLkAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAALJSURBVFhH1dhZTxNRGMbx8/WMC+7fx30D3DC4a3BBNCGCW7yQCxNEFChLoWDUQpGty0yltLO05PV5zrQ4UwsNmkjn4ncxnWnPf4bOOVOUiKh4pE2SH46GBnvZvRFespdDo3ICKjlwRErWgmYlextepZXdXnwhIdZyF3SGQJfu1fGpgcPYiIu12AH3Q6BD97JbpT4eklL+u1g/bom1cKPxoZO97FapQcZ/xYvXxJq/WqWtCo+ph8dV3n8ZLsFFaIUWaIYLcB7OwVk4A6fhFJwsOwHH4Rj4mtDJXnar9KeDUlr7gh0YKIEBGh062ctulf58ABszYs3hisziSjQ6dLKX3SozhPjctLcjjj9do0Mne9kd8vjh/diI4UXcNHHcMI0OnexlN+KbyvG462sdXFeNq7MlXiTgxdoO/V6Mh04vvink8cZIJZ7feZ7ANvGkN5RnhADM535zhHl+S5j5qvGz9Hj8zseE3cqI7POmygQWkwQWk7p4HBeezXBh8rtSg2/Rqal6cSSuQ9747GW3Mkb3llfYdrheA5fkem5i6fbjowbd9izSnQBbuxu05HcvQH+e7mnXvcYo48cYX34wW3rg83BL9tKjoOXHPp1lTzwr1FXl6Z+Sfs+CVvBEyS4+mKGX3coYR3xhXqyVbngelKSezaV6xNZ6f0vTi7KXYmcqXmlOwGtxjM288XnrfZ7u6ta97Fbm+B7968RKvxPb6BPbfC9Otl/c3KC4a0Pi5kcg8pdG/wHGXRsWBx3Oz37dZWf60Nmne9mtzAnEu4ZY2WGxVyNi58bEyU+IW5iCGMxI0f7/XAv0+JPoieou3YdO9rJbmdHdUiquanb+mzgFj2vFtaIzu2Nc22uoNLGv0mpGEZ+YbhFzEtNlMS+lUg6wcz0r6+smGJCB9A7guBzfRA+wS/flxZxqkkSsVfS/PuZjzZKN7goN9oqI+gWobEsFgRiAlAAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_CalendarComboDateButton_Normal" imageBackgroundStyle="Stretched" backColorAlpha="Transparent" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAOgEAAAKJUE5HDQoaCgAAAA1JSERSAAAALwAAABgIBgAAAGpYpLkAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACwSURBVFhH3dg7FoIwEIXh7H8r7oDO0kr06PEBCPGJgoqIojZjkuKmcQFeir+YOVN89SgRUb0gFLas28H7Qy36/KHJeq3b4X8d/HseX5oFWcBn5Zsujy/Mgizg0+JFl8efWrqAXx1buoBP8idd3cDH+YMu4KNDQ1c38Mt9Qxfwi92dLuDn25ou4Gebmi7gp+sbXcBPdEUX8OPsShfwIzOwBXwwiCVMLzRZr8Pzvj5EfQE37w8WaXsrmwAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_CalendarComboDateButton_Pressed" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAOAIAAAKJUE5HDQoaCgAAAA1JSERSAAAALwAAABgIBgAAAGpYpLkAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAGuSURBVFhH1dhtK0NxGMfx6/2MEIlIyM3LUBIlEQmbmW3G7pKSJHLzKnh+paVoSKiVlO0drM3MzeX6nZ2VY/NQnf+D77PzP+dznVOn/zkkIpQ69gkveIwJXrgpdaLwRY/keUQKqTEpXIxL8WLCdcEFH5zwwk3sbZD8+aiUrqelfDcv5ftFeX9Ycl1wwQcnvHAT+xqleDlpHfCRCcjnU1ALubCg5YMTXriJ/U3ymp6uwJ8j8vUSdW3wwQkv3MSBZindzFqT1S6IVcrWJtm4s1y1xL9lDaBOeOEmDrZI6XbOejR14RY+bqcnsJNs0lnud7gghtFBc1FtQ1vXItqaFtZCWtBuVQtoK5pfW7bTN0vOq1XxIcsLN3G49Q/8jwFcdedtvLqJI21m4tVNvNFuJl7dhuNjHWbi1U2c7DQTr27D8ZtdZuLVTbzVbSZe3cTbPWbi1U2802smXt3Eu30OfL1FbsmBVzfxXr+ZeHUT7w84tsT1FrmlCt7eEqub+GBQildT8p7xG/ExAie8cFPqNCF8OCTF9Iy84Tvx0efa4IMTXrgrvz7OksJHw8YEr4jQN1VGbAhOWnI8AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="Default_CalendarComboDropDownButton_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="1, 1, 1, 1">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAABwEAAAKJUE5HDQoaCgAAAA1JSERSAAAADgAAABUIBgAAAHZZ34kAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADq8AAA6vAXERQ+4AAAB9SURBVDhPncuhDoJQAIZRnvE+FLOQKBSLhUKQaKWS2KwmgcEAGVPHmIPrZrg/9TOceLxXYew/ftF+RkRxqREXt/mGKL6viOIzRxSnDFEcL4jicEYUuxhxcb37iGJ5QBSrAFGsQ0SxiRDF9ogodidEsY+RXUwQxUeKuMgZ+wWqsAJhjtoZIgAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_CalendarComboDropDownButton_Pressed" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="1, 1, 1, 1">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAsgAAAAKJUE5HDQoaCgAAAA1JSERSAAAADgAAABUIBgAAAHZZ34kAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADq8AAA6vAXERQ+4AAAAoSURBVDhPYzhUrv2fHAzW+P9JF0l4VCMePKoRDx7ViAcPRY2kY+3/AONlqeSltQNXAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ExplorerBarGroupHeader_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAKQEAAAJHSUY4OWEcABwAgwAAhZ6/yd7zx93y4e34+v3+1ub2zuD05e/5////3On30eP1yt7yAAAAAAAAAAAAAAAAIf8LTkVUU0NBUEUyLjADAQEAACH5BAAAAAAALAAAAAAcABwAAAjDAAEIHEiwoMGDAAgoXMiwocOGAgkYmEjRwIKLATJq3CigowACEStSvLhgo8kAHj+GFGkR40mNKUEmZNmy5MuMMVeKJHkTp0eZElny7JlzplCXN4sG3Yn0pVKaNElKJQlUgdWrWLFCnQi0gNevYMFmzQo0gVmzYdOqLVD2rNu3cN8CHUC3rt27duVGPMC3r9+/ffHKRAC4sOG+gw8r/pt4sePGjhVDjmx4MmXAli/7zayZL+fOgxGIHk2a9EOGCFOrFhgQADsLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ExplorerBarGroupHeader_Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAdAEAAAJHSUY4OWEcABwAhAAAhZ6/v9XsvNDp1+bz4u33zt3v7fT6zeH0v9Pq1Oj3w9juw9bqxtvw6fP73ef00eX25+/37PX86/L5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIf8LTkVUU0NBUEUyLjADAQEAACH5BAAAAAAALAAAAAAcABwAAAjeAAEIHEiwoMGDACIoXMiwocOGAhskmEixosWLFBtEfMCxo8ePIDtqBADhgMmTKFOqPAlBIIGVMGMScMmgps2bOHPanAmAgIKfQIMKHQqUp4MASJMqXco0qQOBDgRInUq1qtWpTwFEvcqVa9atXcNihUoVgdmzaNNW/Zq2rdu2bNMumEu37gK3WQnY3ct3L0+fRAMH/quzsGGeEAooXsy4sePFLUk+nkw5cmLKmBlbzsy5QGQJIUOLliBQwoDTqFOrXo2aNADTrGPHdg1btu3WAg3o3s27t+/eCIMLFxgQADsLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="Default_GridColumnHeader_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="1, 1, 1, 1">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAACAEAAAKJUE5HDQoaCgAAAA1JSERSAAAADAAAABQIBgAAALnw3BEAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADmkAAA5pASsAzo8AAAB+SURBVDhPjcuhDoJgAABhn4tXMtlMNhPNZCPZTDYTiWaymWBMB5PJZDidlN8xN65e+NrdrE6jEPK5Mrb/oVgoDOVSYbiuFIbbWmGoYoWh3igM963C0CQKw2OnMLR7heF5UBi6o8LQpwrDK1MY3ieF4XNWGL4XhWHIlWnwovADalU0ptp9WcwAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_GridColumnHeader_Normal" imageBackgroundStyle="Stretched" borderAlpha="Transparent" imageBackgroundStretchMargins="1, 1, 1, 1">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAqQAAAAKJUE5HDQoaCgAAAA1JSERSAAAACAAAAAkIBgAAAA9TbS4AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADmkAAA5pASsAzo8AAAAfSURBVChTY1iw4fh/fJjh1vOv/3HhYaQARODGx/8DALcE/KDEN25PAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ProgressBarFill_Normal" imageBackgroundStyle="Stretched" borderAlpha="Transparent" imageBackgroundStretchMargins="1, 2, 1, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAGQEAAAKJUE5HDQoaCgAAAA1JSERSAAAACgAAAAwIBgAAAFtrLKAAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADq8AAA6vAXERQ+4AAACPSURBVChTYxBr9vtPDAYrXH5lHRwvQ2LDMFxh7eEWMK453IwVwxVm7M+C4uz/6UAaHcMVxu9PgePYfUn/4/Yl/4/bj8BwhSBJfBiu0OaiBxxbn3eH4AtQfNEdoVD8ljocS9xExSAxuEKFZ+ZI2AwVPzVDKJR9pfNf7jUEy2LBcIWiL+UQ+AUmhiskjP3+AwCosg8vnAxhNAAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ScheduleDayHeader_Normal" imageBackgroundStyle="Stretched">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAhgAAAAJHSUY4OWEKABQAggAA/9hr/+Z0/+Bw/+15/9xtAAAAAAAAAAAAIf8LTkVUU0NBUEUyLjADAQEAACH5BAAAAAAALAAAAAAKABQAAAg4AAcIHEiwYMEACBMqXMiwIUIBECNKnEixIkQCGDNqBMCxo8ePIENy1EiSgMWTKCM6XMkyoUGCAQEAOwsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ScrollBarButton_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAEwEAAAKJUE5HDQoaCgAAAA1JSERSAAAADwAAABEIBgAAAAIK9qEAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACJSURBVDhP7dM9DoJAEIZh7n8YLbDAamkoNmETjkAMxkLFAvZ/dj8XuMBKZ0LxFpPMM90UvO3qU1nhfLn+UIWGC1Ysw3v8gCiAQooI3ns452Gtg7EW2hgobSCVhpQKw/2xHlhxjBEhxGw8zXI/nlMHPvA/4OdrTCjBTNzfhg1z0bH8f172thou2Bc69bXTiMCytQAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ScrollBarButton_Normal" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAABgEAAAKJUE5HDQoaCgAAAA1JSERSAAAADwAAABEIBgAAAAIK9qEAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAB8SURBVDhP7dNBCoNADIVh73+gFtpF3ElF6bqIiLZVJ8nEIzyNvUCnu4KLH8KQL7vJbtU9P10I52v+ZQTfL8qaMn8Ynm+YGWL0IlQjRBUiChYBsyAwYw5eQNN2+4Edmy1JeJzC73iaD3zg/8D98ErCj6b94O0/kw+pFWVNK4vMmlN5p6GNAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ScrollBarButton_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA6AAAAAKJUE5HDQoaCgAAAA1JSERSAAAADwAAABEIBgAAAAIK9qEAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAABeSURBVDhPY9h/+NiBdZt3/F+/ZSfRGKR+/+Hj+xlAnGs3bv1/+Ogx0fjq9ZtgA8CasSnAhx88fES+ZhAe1UwiHtVMIqaOZlAuwaYAF7505TpEMyhfghik4v2Hj+8HAORff/9VmYyAAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_SpinButton_HotTracked" backColorAlpha="Transparent" borderAlpha="Transparent">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAADwEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAAEZ0FNQQAAsY8L/GEFAAAACXBIWXMAAAsMAAALDAE/QCLIAAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1M0BrLToAAAAIlJREFUOE+l1DEKwkAUhOE9V86XIk0KixQWaSxsPIPkCLZp7ZYElYQVkQgjLLxlYZodUvzNg/nK5wA4P1TYkUvAL4xyto2IHeHr4nIoIZgaOUbmVo6Rx0GOkWcnx8jrKMfI0ssxsp7kGAlnOUbeFzlGPlc5Rr43OUIitN2Ls01CckjJtrv/CQD3Bx/N5CvL0t0dAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_SpinButton_Pressed" backColorAlpha="Transparent">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA/QAAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAAEZ0FNQQAAsY8L/GEFAAAACXBIWXMAAAsMAAALDAE/QCLIAAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1M0BrLToAAAAHdJREFUOE9j+P//P8Ohbsv/5GKQfrgBPy4WkYxhesGGwAT/P6wiGiMbBDcELPm8gXgMNWjUECQ8Egx50vX//6uJxGOgekxDXi74///9SuIxUD2mIW+2/f//+SDxGKgewxCwQR/PEo1heuCGIBtECobppUJ58p8BANlfm7LzBtw+AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemHorizontalBottom_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 0, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAGgEAAAKJUE5HDQoaCgAAAA1JSERSAAAACwAAABcIBgAAAN24tcYAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACQSURBVDhP7YvNDsFQGETr/V+GNDaSrukNCUJRP133u8siYWHcIRE67cK+i7M5cyYaDEc4X++YbMpWuLOrxdZAF3fxH3EahnFWCvQSu50h3Sr0Es9yj2luAr3Ei4PHfK/QS7w8heFoAr3E68JjFYY69D9xdbm9RFaYQP+J48Q9+l+HJhiGDhHwPlC0wTBOXO8JXGh5lJOJTNcAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemHorizontalBottom_Selected" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 0, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAMwEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABgIBgAAAPOgfQwAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACpSURBVDhP7ZM9C8IwGITT//+H1MVZF8FBBwvtJnSxftYKyXnXKihG8JWMDTwQkrsnGRI3mm2wu/i/UNct823QpG68CXXYhQOgW4S6Cdz4ne50jadgz0ULEpAsoeDKDQMfggMXLaQXHNsAC+kFpxawkF5wvgEWBsEgEG+C8TznSwzRYAxl2RG9YFVUfroouk8SK7yijLLsiF4g1mUVJrQ+rvYVZZgVGQB3BzKyg4rUUV1tAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemHorizontalTop_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 0">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAEQEAAAKJUE5HDQoaCgAAAA1JSERSAAAACwAAABcIBgAAAN24tcYAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACHSURBVDhP7cu7CoNAEIXhff+3EUkjWHvBgIZEEy+x3tk2WKQ5cZDIwlHwASz+5sw3BoAJ4xzBJdpN7+pW+Jm+u/0fjA/rUSj/YcWP0eH+5nQnfJsP1SCU7oSr3qHsON0JX1uHohVKd8L5S5A9Od0JZ40gqS2lO+G0sXOykT2x14kP4e0WHOEH5H56X/2y7jwAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemHorizontalTop_Selected" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 0">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAJgEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABgIBgAAAPOgfQwAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACcSURBVDhP7ZO9CoMwGEX7/k9kXZztIjjooGC3gov/Wgt6mxswixL8JKPDCSS59yRD8gCgSd8l/FcOL8ysMMPs1tNDUpQIogLdvGL4wQozzLJjBE9l7U+UN5hlxwh4taOgDXZuwS3YCfqZL+w87gX8JBLcC9rvKsK9oFGLEvaCSW0IcC+o1aIEI4jzj57U0yKCHXb16dW4XMILM/wBiQmTNG1C29MAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemVerticalLeft_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 0, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAABgEAAAKJUE5HDQoaCgAAAA1JSERSAAAAFwAAAAsIBgAAAJQ+FkAAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAB8SURBVDhPY9i89xQTEP/fuHUX1THIcDDjy7dfVMcMyAZfvf/6/6V7r/9fvPP6/9lbr/+fuvnq/8nrr/6fuP6SLAw3/Nr9V/+vgAy+++r/hduv/5+5BTT4xqv/x6+BFIIsIBWPGo4V08fwn2ALqJ5aEOkcYgE1MQ1z6K7/AD3peNvU4NvpAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemVerticalLeft_Selected" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 0, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAJQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAGAAAABAIBgAAAAwkv5UAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACbSURBVDhP7ZS7CgIxEEWT//8htbF2G8FCiw2snbCNr/WxQnK9k7GMBCEBiw2cIsMwhwnkGgBV2LhDmK1avWy73s8bJ4WS4Hj3MLt9H5brDsMYcHshyzAqV/YLl6dyFh7KKeJVsGhcbEwNS/GzQFZJDfrGJMgyCbL8n6D6R2NUoGpUUGAZdmDYQZ6rJFHwSVNLgWWxCIaHcU1JizePuoOKnDTMkwAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemVerticalRight_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="0, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAEAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAFwAAAAsIBgAAAJQ+FkAAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACGSURBVDhPY9i4ddd/auPNe0+BMCPDl2+//lMbwyxgOHH95X9y8Mnrr/6fuvnq/9lbr/9fvPP6/6V7r/9fvf8axQKo4a9IxsevAS248er/mVuv/l+4DbTg7qv/V4AWXLv/atTwgTecdIwvtXz++hNiOIhDTQwyeAPQYGA6/0erHPrv////DAA7x3mUz6R0rAAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemVerticalRight_Selected" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="0, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAJAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAGAAAABAIBgAAAAwkv5UAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACaSURBVDhP7dS7CsJAFIThw77/Axkba9MELEzhQtIJ23iLlygn4xljxMo0u4WQ4oMUWX4S2JHZcoOV3yoASUF2FwUjRmPJcq/rKjw+gf2VOhwGtw7Ht1Pba1q8nO/jGnt/UVQo66BJAsQz89wjWYDsd02B36bAqP8O8Eyyi/Y1FX2AnxKTjR1s7Bhww1zb7AmfXQwWcBZwZR3kCXQ3gyuyjzfbAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TileCloseButton_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAnQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAIAAAABcIBgAAAGoFTeEAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEoSURBVEhLzZY9TwJBFEXnp/E/sKOko1ajNCZ2llZGowZ7e1sSY7EBtfIj4heIGqLL7qw41/vGDcMqnfJCcZptzileZq8BYNJ2BUmzpIo4xW3SaAHZWRXu7QT46OY8zhRxiVPcRmpc3ALiY+B5P2dvCrt/46VBDr4ZRt4pbh+A7A542gJ6mzr0d7wzBNgO0N3Qhc6JgGvgfk0XOkNAegncrupC54+AZV3mLOAC6CzpQue8BSzqUgzgDUyrnCW/j3BFl2LAFT/WdaEzBPiXcF2XwkuYPfAHsa0LnSFg1AcGh7rQORHwCrwf6UJnCPgccCRwkAybOiRcX3SOA5y94VTiHdh2DhfSv3MaGPW80wekURn2vAaX8DV0sQriEqe4/Sq2Lf1VLE4A5gsJHfWEHjwm/QAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TileStateChangeButtonLarge_HotTracked" backColor="212, 232, 255" borderColor="Gainsboro" imageBackgroundStyle="Stretched" backGradientStyle="None" borderColor3DBase="Black" backHatchStyle="None" borderColor2="WhiteSmoke">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAnQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAIAAAABcIBgAAAGoFTeEAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEoSURBVEhLzZY9TwJBFEXnp/E/sKOko1ajNCZ2llZGowZ7e1sSY7EBtfIj4heIGqLL7qw41/vGDcMqnfJCcZptzileZq8BYNJ2BUmzpIo4xW3SaAHZWRXu7QT46OY8zhRxiVPcRmpc3ALiY+B5P2dvCrt/46VBDr4ZRt4pbh+A7A542gJ6mzr0d7wzBNgO0N3Qhc6JgGvgfk0XOkNAegncrupC54+AZV3mLOAC6CzpQue8BSzqUgzgDUyrnCW/j3BFl2LAFT/WdaEzBPiXcF2XwkuYPfAHsa0LnSFg1AcGh7rQORHwCrwf6UJnCPgccCRwkAybOiRcX3SOA5y94VTiHdh2DhfSv3MaGPW80wekURn2vAaX8DV0sQriEqe4/Sq2Lf1VLE4A5gsJHfWEHjwm/QAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TileStateChangeButtonNormal_HotTracked" imageBackgroundStyle="Stretched">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAnQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAIAAAABcIBgAAAGoFTeEAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEoSURBVEhLzZY9TwJBFEXnp/E/sKOko1ajNCZ2llZGowZ7e1sSY7EBtfIj4heIGqLL7qw41/vGDcMqnfJCcZptzileZq8BYNJ2BUmzpIo4xW3SaAHZWRXu7QT46OY8zhRxiVPcRmpc3ALiY+B5P2dvCrt/46VBDr4ZRt4pbh+A7A542gJ6mzr0d7wzBNgO0N3Qhc6JgGvgfk0XOkNAegncrupC54+AZV3mLOAC6CzpQue8BSzqUgzgDUyrnCW/j3BFl2LAFT/WdaEzBPiXcF2XwkuYPfAHsa0LnSFg1AcGh7rQORHwCrwf6UJnCPgccCRwkAybOiRcX3SOA5y94VTiHdh2DhfSv3MaGPW80wekURn2vAaX8DV0sQriEqe4/Sq2Lf1VLE4A5gsJHfWEHjwm/QAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TrackBarButton_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAATwIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABAIBgAAAB/z/2EAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADmkAAA5pASsAzo8AAAHFSURBVDhPjZJPTxNRFMXnm7QfQUBQRFjIisRvpskUIiCwGAm6UBMCEl0ICyIopQoDQggx/DF02saSdtqhzGtn2lzOea2k00XTxS+5vfecM6/3PUNEIhS+jcVBAthAWrBmL96pj/wobI0m3ORzUc6ShN6hNKpZDWv2OKOm3dNmfmpXzl5K3b+Q8OZQqtdr4mfeAAv1Fwkrx3pGDbWRgOL2iHl7/gKCc6nmV8VPz4HXHcxjtiZ19VeopUcHFL8/iZdSEy3zCoQzXaldf9Zaeug1ij+GLeVYOPbvluhVd5xZaI+wE0voRcBjO/T2ml+/muoJnoIeeg135xE2nUb6NIaJnlCZBe2h13CTDMiIymLjDpbVAyq7qD30Gu7ukM1rqxXWdUgvBO6mvmp6ETBoqdxbbPaPqH8fwPvu5D/iKi9E5d4hYNAySqmHsfL+uD5SUN7Bo/nUlaCc0lp66NUPqfRzwPTTpjSCvAQ3+1ItroOvHWxIUMHzhoZaeu5fog751W/7zpQWNGo5CW9P8LWkBF4K9anuNc2TQu1/331AM6TP9A6e4ajL+J+X0qh7GtbscUZNuycSQMp7D2LABDaQFqzZi0X1YtwBTqtIy4MYCoQAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="Default_TrackBarButton_Normal" imageBackgroundStyle="Centered" borderAlpha="Transparent">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAuQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABAIBgAAAB/z/2EAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADmkAAA5pASsAzo8AAAEvSURBVDhPnZJPSwJRHEX9Jvot2/Rn2UayZW2KynJpQbTITRJEIjRRCJqIJA7lJA2mDUnR7Z3HjDjhYsbFgTe/d899M8PLSIqxsrmXM2wbHINCWDPL/c/HHghtbBV1cXWnTn+oz+mvhTUz9sjMO/Oyc3xWlecH+gh+FsIeGbKxAjMoFE+r8iffiSCLYwvMIrduXq3vjfU2miaCLA4uBfvlSl2v/lcqcHDtt9+3XLnDIBU4uBSo502WAtcWdF/GSxEVOLePz2q7o1Tg4NqfWDq/UbPnpwIHl4LsWv5ITmugRvc9EWRxcGcXabdU0UNnmAiyOLObGJY4OyeXqjVcOU/eQtgjQzbyZgVhSWE1f6iD8rUqtbbqzYGFNTP2opMjYgVgAllCnGJAsCeGs2w8r8wffiZHKJKORMMAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="Default_TrackBarButton_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAbQIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABAIBgAAAB/z/2EAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADmkAAA5pASsAzo8AAAHjSURBVDhPjZJNTxNRGIXvP2l/BUYjwajRCCbucOHGj6hoKSSggKYFFbASIYMxxqiJrIkrF7JRk06B4UPNKCi1FTI1Gh1i2c10AS3H91xpw5iGzOJpbt57ntN7J1cBCJBJNkSFccEWsAPXnEX/z9cWhTcXVGZg3/hcqhHOyzP4lb6E39NtGq454x4zzAYKtHx7v52fbBXhCooLcXifrmFzpQ+b2T54H7vxZ75d7zHDbLVE/2QGDxj5F6exbsXgL/egnE/UxV+6Dnc2Bmbp6AJz+GDUGj0Md+YqSsu9qHzr3xNvqUdn6dBVZqpxwpk6i+JiJyqrt1BZ25vy6oDO0qGrzJEm252JofTlJradwVB4n2/IKWKgq8z7h+Re7ahw8/twKLbW7miHrjLH5P6WFBSGgB+pUJSdIe3QVaZxxHatOEq5fuDnSCi8lYQUxEFXmQ+OThReX8TGh25pv1dX2M22ZIrvukCHrjIfHotYT1vgznWEOoWfTUo2Djp09UNKPzpu5F6dw/p857+SOifhP/vZhGQ6wCyd2kvks0w/PmHnp87r9uL7LvhfkygX7mJLPhjvvCEz7uUkw2zgKddKnjQbs89PwXl7WX+k3XDGPWaqcqCgSvrZyYhgCLaAHbjmLBLMQ/0FucH8PuBvg3UAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbHorizontal_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAKwEAAAKJUE5HDQoaCgAAAA1JSERSAAAACQAAAA0IBgAAAHsARAYAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAChSURBVChThYpHEsIwEAT3OeT8T9KPyNFn4A5ccUAcoLzsbNkqYRVw6JJ2uuk26fE/KJx2OX2cvgJP4awjx5Gf17EHdniK5m1OjUQXEQWww1O0aGXRyEMj8RQvm1k09MAOT/Gq8TsST8m6LseBX+eBB3Z4SjY1PYphvsETM9N9W3XCvv1jh9dIw13FSg3kzp2NgNmXNcDr7h8RMEFJHndjegPrKIOOCmG1kQAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbHorizontal_Normal" imageBackgroundStyle="Centered" imageAlpha="Transparent">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA/wAAAAKJUE5HDQoaCgAAAA1JSERSAAAACQAAAA0IBgAAAHsARAYAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAB1SURBVChThcqxCYAwEIXh7OwITuEM9i7gDBYWIgoKATWEECUinlwgp1EOi7957xNJmsFfHrnjZCNk3cFGSK87G6HZODZCUm1shIbJshHqpGEjlBclNKP+hLtHADese0UFgL9HT1i1SwQihAX4BFiEsDcAAHEBzzuA0sZWwMgAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbHorizontal_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAASAEAAAKJUE5HDQoaCgAAAA1JSERSAAAACQAAAA0IBgAAAHsARAYAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAC+SURBVChThYrNCkFhFEXPA0mSSJLkQWSiDGVApOQ/UkrJwxidkpGJmJHfgZkiRS6bfcuNbjeD1fm+tbZozoV/iBbcOGncEXbRogfncRK3acYGPbtoyYvrLAtjUbJBzy5a8QH7liPsojU/nrumI+yizQCe24Yj7KLtIO7LMh6bug16dtFOCJd53sRY1yw+jl2A97AbxnGSNjFWVetNz26OzGEvgsMoZcH/p1kjov0o1sMEeL/9z4joIPY+3w7yAqoWYNmk2bA5AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbVertical_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAADwEAAAKJUE5HDQoaCgAAAA1JSERSAAAADQAAAAkIBgAAAOl6pmoAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACFSURBVChTY/hwyOT/x8PG/z8eMfr/6ajh/0/HDP5/Pq7///MJvf9fTur+/3JK5z8QMCBjsKZ/328B8c3//76h4xtg/PW0NopGuKbfzyf///0MhCcB8UQw/vN0AkLjGS24RoKakDV+O6sJ1kiUpj9P+xEaz2n8p41NMA1Y/URy6JEWT/8ZAKe2g47t9hUhAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbVertical_Normal" imageBackgroundStyle="Centered" imageAlpha="Transparent">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA6AAAAAKJUE5HDQoaCgAAAA1JSERSAAAADQAAAAkIBgAAAOl6pmoAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAABeSURBVChTYyhunvmfEAYCBmQM1vTzzz+cePW2wxgawZo+f/+NF6NrBGt68+kHQYysEazp2duvRGGYRrCmBy8+E8QYNt18/AEvxuqny/fe4sToGuCaCGFkDf///2cAAHezfrdmwSeBAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_UltraButtonBase_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAArQEAAAKJUE5HDQoaCgAAAA1JSERSAAAADAAAABoIBgAAAIP6vWEAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADmkAAA5pASsAzo8AAAEjSURBVDhPpZQ7S8NQGIb9B+1P6v9xc9YqioujizgVHQQH0dGtjgUpWOplUtGk1MbE4iWNSfW8vl9AT44NX4cMz3Se55Bzy0LSadTJCcEcxKlL0M6uF2Heu8B0RJ5mkDFxxJUAJu4D8TkQHQDhnsvLITDp5Y64eYBsADy3gGC3nHA/d2yQesBoW4dOIXgAhls6dGzweQ8MNnToFII7wF/VofMvaOpUDG4Bb0WHTtVgWadiwEV7XJjG7C6t6bgBT9pf13FPWq7Gpo4TZD6v8Y4OHRtMA2B8pEPHBl8R8HaqQ6cQjIGPMx06Nvh+5bvlm550ykn4g6DzF5j0kd845Ku65G5cuKRXnD3Ind+gnd4swSS8HiYuRcbEEVeCGjmWeg50GrUf0eQqT6JiuT0AAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="Default_UltraButtonBase_Normal" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAoQEAAAKJUE5HDQoaCgAAAA1JSERSAAAADAAAABoIBgAAAIP6vWEAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADmkAAA5pASsAzo8AAAEXSURBVDhPjcvJTgJREIXhegN4/5cQw8LoQtQgIgJCM/RAd2MUjSZuBHseinuIy0LqJv/i5nxF4fqlaWqZ+EQwTQrCdedt885xkvCxhw0Glvwg5DTL/qbjDwaWVn7AdV2rgiVv5YujFOzhoKpqVYcD11uZT6UKlhzX47KsVMGS7bjiKAVLS9vhoihVwdJiaYujFCzNF0txlIKl2XzBeV6ogiVrNhdHKViaWjPOskIVLE2mlvnkqmBp/DzhNM1VwdJwNBZHKVgaDEecJJkqWOoPnsRRCpZ6/UeO41QVLHV7DxxFiSpYuuve8+43VgVL1ze3/Pn1zdtd/G8wsHRxedXB5evmg3+2kRg2GFhqnbcbpjMTn8iYdmMPNMObUz7dvesAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="Default_UltraButtonBase_Pressed" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAxgEAAAKJUE5HDQoaCgAAAA1JSERSAAAADAAAABoIBgAAAIP6vWEAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADmkAAA5pASsAzo8AAAE8SURBVDhPjdTLLkNRGMXx9Qbte7iL+/3aN/AEEhIdEAPVEKLaVAiRCiE8A96gyScRqYRIihniFZRWk2Nb3zaSnLTfYA322b//Hh7IUkOUu+RcnamJQhKN+WJuxH1cTbuvm9nQ6Z0atZBkkytdz7jybdxV7uZd5X7h//itXJjzRi1kpdm/Un1adtXnZPgeE96ohay2+EPwkqo5H9BC1lr94ec9W3M+oIWst9kDWshGuz2ghaQ67AEtJN1pD2ghmS57QAvJdtsDWshWjz2ghWz32gNayE6fPaCF7PbbA1rI3oA9oIXsD9oDWkhuyB7QQg6G7QEt5HDUHtBCjsbsAS3keNwfgrdMKNQFr5t/AS3kZMJ9FuLu+2GRF+lQrD8DNWohp5P54vmUK/GDvhI2vVOjFnIWi3AXnKszmljkF/7NyQ8wzYMhAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
  </resources>
</styleLibrary>