﻿<?xml version="1.0" encoding="utf-8"?>
<styleLibrary office2007ColorScheme="Black" office2007CustomBlendColor="">
  <annotation>
    <lastModified>2013-05-25T05:14:45</lastModified>
  </annotation>
  <styleSets defaultStyleSet="Default">
    <styleSet name="Default" viewStyle="Office2007">
      <componentStyles>
        <componentStyle name="Inbox Button">
          <properties>
            <property name="BackColor" colorCategory="{Default}">237, 238, 240</property>
          </properties>
        </componentStyle>
        <componentStyle name="Inbox GroupBox">
          <properties>
            <property name="BackColor" colorCategory="{Default}">237, 238, 240</property>
          </properties>
        </componentStyle>
        <componentStyle name="Inbox ListView">
          <properties>
            <property name="BorderStyle" colorCategory="{Default}">FixedSingle</property>
            <property name="Font" colorCategory="{Default}">Segoe UI, 8.25pt</property>
            <property name="View" colorCategory="{Default}">Details</property>
          </properties>
        </componentStyle>
        <componentStyle name="Inbox MonthCalendar">
          <properties>
            <property name="BackColor" colorCategory="{Default}">White</property>
            <property name="Font" colorCategory="{Default}">Segoe UI, 8.25pt</property>
            <property name="ForeColor" colorCategory="{Default}">64, 64, 64</property>
            <property name="TitleBackColor" colorCategory="{Default}">220, 222, 227</property>
            <property name="TitleForeColor" colorCategory="{Default}">64, 64, 64</property>
          </properties>
        </componentStyle>
        <componentStyle name="Inbox Panel">
          <properties>
            <property name="BackColor" colorCategory="{Default}">237, 238, 240</property>
          </properties>
        </componentStyle>
        <componentStyle name="Inbox UserControl">
          <properties>
            <property name="BackColor" colorCategory="{Default}">237, 238, 240</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraCalculator" useFlatMode="True" />
        <componentStyle name="UltraDockManager" useFlatMode="True">
          <properties>
            <property name="GroupPaneTabStyle" colorCategory="{Default}">Flat</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraDropDownButton" useFlatMode="True" />
        <componentStyle name="UltraExplorerBar" useFlatMode="True" />
        <componentStyle name="UltraGrid" useFlatMode="True" />
        <componentStyle name="UltraListView" useFlatMode="True" />
        <componentStyle name="UltraTabControl">
          <properties>
            <property name="Style" colorCategory="{Default}">Office2007Ribbon</property>
            <property name="UseHotTracking" colorCategory="{Default}">True</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraTabStripControl" buttonStyle="FlatBorderless">
          <properties>
            <property name="Style" colorCategory="{Default}">Office2007Ribbon</property>
            <property name="UseHotTracking" colorCategory="{Default}">True</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraToolbarsManager" useOsThemes="False" />
        <componentStyle name="UltraTree" buttonStyle="FlatBorderless" useFlatMode="True" />
      </componentStyles>
      <styles>
        <style role="Base">
          <states>
            <state name="Normal" fontName="Segoe UI" themedElementAlpha="Transparent" />
            <state name="HotTracked" fontName="Segoe UI" />
          </states>
        </style>
        <style role="DesktopAlertButton">
          <states>
            <state name="Normal" backColor="Transparent" borderColor="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="Transparent" borderColor="Transparent" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="DesktopAlertCloseButton">
          <states>
            <state name="Normal" foreColor="45, 45, 45" />
            <state name="HotTracked">
              <resources>
                <name>DesktopAlertDropDownButton_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="DesktopAlertControlArea">
          <states>
            <state name="Normal">
              <resources>
                <name>Button</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="DesktopAlertDropDownButton">
          <states>
            <state name="Normal" backColor="Transparent" foreColor="45, 45, 45" backGradientStyle="None" backHatchStyle="None">
              <resources>
                <name>spacer</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>DesktopAlertDropDownButton_HotTracked</name>
                <name>spacer</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="DesktopAlertGripArea">
          <states>
            <state name="Normal">
              <resources>
                <name>ScrollBarTrackHorizontalOffice2K7Black_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="DesktopAlertLink">
          <states>
            <state name="HotTracked" foreColor="Black" />
          </states>
        </style>
        <style role="DesktopAlertPinButton">
          <states>
            <state name="Normal" foreColor="45, 45, 45">
              <resources>
                <name>spacer</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>DesktopAlertDropDownButton_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="DockSlidingGroupHeaderHorizontal">
          <states>
            <state name="Normal" foreColor="White" />
          </states>
        </style>
        <style role="DotNetMonthDropDown">
          <states>
            <state name="Normal" backColor="White" fontName="Segoe UI" fontSize="8.25" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="DotNetMonthDropDownTitle">
          <states>
            <state name="Normal" backColor="220, 222, 227" foreColor="64, 64, 64" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="EditorButton">
          <states>
            <state name="Normal" borderColor="Office2007.MainMenuToolDisabledForecolor" />
          </states>
        </style>
        <style role="EditorControl">
          <states>
            <state name="Normal" foreColor="Black" foreColorDisabled="60, 60, 60" backColorDisabled="Office2007.GroupBoxContentAreaGradientLight" />
            <state name="ReadOnly" backColor="240, 240, 240" foreColor="30, 30, 30" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ExpansionIndicator">
          <states>
            <state name="Normal">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAACQEAAAKJUE5HDQoaCgAAAA1JSERSAAAACwAAAAsIBgAAAKmsdyYAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAcklEQVQoU4XQvQ2AIBCG4RvJMRzDkRyB0pIRKC0pLSktKek+OQx/hX4kb3LFkyMgAMRYh9LR2zXTU1dgOwmItQgE7Y4IeVjW7d3KoG84X8egvwbMYMP6kAq/sPMj/tmocMIMzphAd46YQFtx/mHRgQVAHvU+julnFiLbAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
            </state>
            <state name="Expanded">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAACQEAAAKJUE5HDQoaCgAAAA1JSERSAAAACwAAAAsIBgAAAKmsdyYAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAcklEQVQoU4XQvQ2AIBCG4RvJMRzDkRyB0pIRKC0pLSktKek+OQx/hX4kb3LFkyMgAMRYh9LR2zXTU1dgOwmItQgE7Y4IeVjW7d3KoG84X8egvwbMYMP6kAq/sPMj/tmocMIMzphAd46YQFtx/mHRgQVAHvU+julnFiLbAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
            </state>
          </states>
        </style>
        <style role="ExplorerBarControlArea" borderStyle="None" />
        <style role="ExplorerBarGroupHeader">
          <states>
            <state name="Normal" borderColor="Transparent" />
          </states>
        </style>
        <style role="GridAddNewBox">
          <states>
            <state name="Normal">
              <resources>
                <name>BackgroundAndBorder</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="GridAddNewBoxButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Button</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="GridAddNewBoxPrompt">
          <states>
            <state name="Normal" foreColor="60, 60, 60" />
          </states>
        </style>
        <style role="GridCardCaption">
          <states>
            <state name="Normal" backColor="235, 233, 237" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridCell">
          <states>
            <state name="Normal" borderColor="195, 195, 195" />
            <state name="EditMode" backColor="Transparent" backGradientStyle="None" backHatchStyle="None">
              <resources>
                <name>GridCellOffice2K7Black_EditMode</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="GridColumnHeader">
          <states>
            <state name="Normal">
              <resources>
                <name>GridColumnHeaderOffice2K7_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>GridColumnHeaderOffice2K7_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="GridControlArea" borderStyle="Solid" buttonStyle="Flat">
          <states>
            <state name="Normal" backColor="232, 232, 232" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupByBoxPrompt">
          <states>
            <state name="Normal" backColor="Transparent" foreColor="96, 96, 96" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridRow">
          <states>
            <state name="Normal" borderColor="195, 195, 195" />
            <state name="Selected">
              <resources>
                <name>GridRowOffice2K7Black_Selected</name>
              </resources>
            </state>
            <state name="Active">
              <resources>
                <name>GridRowOffice2K7Black_Active</name>
              </resources>
            </state>
            <state name="AlternateItem" backColor="Office2007.ContextualTabGroupCaptionGradientLight" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridRowSelector">
          <states>
            <state name="Normal" backColor="232, 232, 232" borderColor="195, 195, 195" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GroupPaneTabItemAreaHorizontalBottom">
          <states>
            <state name="Normal" backColor="97, 97, 97" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ListViewColumnHeader" borderStyle="None">
          <states>
            <state name="Normal">
              <resources>
                <name>GridColumnHeaderOffice2K7_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="MenuSideStrip">
          <states>
            <state name="Normal" foreColor="Black">
              <resources>
                <name>GridColumnHeaderOffice2K7_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ProgressBarFill">
          <states>
            <state name="Normal">
              <resources>
                <name>ProgressBarFill_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ScheduleDay">
          <states>
            <state name="Selected" backColor="232, 234, 236" backGradientStyle="None" backHatchStyle="None" />
            <state name="AlternateItem" backColor="199, 203, 209" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ScheduleDayHeader">
          <states>
            <state name="HotTracked">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA4gcAAAKJUE5HDQoaCgAAAA1JSERSAAAAcQAAABEIBgAAAOqKQ4oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAHS0lEQVRYR+1Y21YjOQzM///X7sxwDZAbCRAI4TKzH9FbVVLZ6oQ9O/C4Zx/qSG6wJVXJbncmr+9/DYFfw8sb8VPYv3Y8v74HXt6H3cvb8Lx/G3b71+EJtuLxGc+Ax90L/Jdh+2TshYen5+H+MXC3hU3cbXfD5uGpgOPdsL5/GtYY395thzVwuyEeh9V6Oyw3D4F1YHF7D9B+Bvdtvtfj2ozBWIrJ2MiBuTivmitz73X0+lir6248kBeDXBXuyKd4Jb/g2ZxXHawNdaJe1k4iWrz453f4fbKFa+LRZ2CKlaIZFK6Jh0S3jygAYEFNwCJcA4khSQmRBtzeP4q8EG87rAwRHwIsEnOJ+DloTs4PMe+1tuM4rnJgLplXzZW511pcn4WUmMlDEzSbvPMWlryOeKYP/ruI9GPcxYSI+9df+U/caSGahNvDAhLMu+45xFNgiiS8dmSCDxIOQDEP233rThbJTg7R3NW54+5AUuJWpHEHwgeCWJKMHUjkLrIQc4znK9ivgHNzLYJrOw5jMrbzcF4119EORU2qDZCgWTc5IBfyk5vgytzF7jS34jl5J/+hQ+gSCL1Ct1//i/gfEZHC5bGZoj3nkSkBsWA9NnVkUjwmkaIZEk7iAUj26Oh0sYCKT9BvRBXoWIPlUUcyRXAKaOItxmx5N8xWnwTmeH5d03EUk7Ezl49yPKzD9bFW1z06YrOpJaS5Sy7J6/Ex27XoulQxfw4TC6eLymin2XaRQrQ42x/UZSGW4c5jARIvi2kXAb5X7saiaefxskLBDhCXDe48EguCSXaSPocIhMQDbpabL8HzvZ4ETTiucuCuLLkZzF01uB6KiRpZa7v8WFQjd2nnLfjUO1P8ds69Q61HIPSyoBAxdpv/MSZlV3AhLBhdk+LxZc2g2VkV0XkpHnx3aO/UXqwuDemLkNxt6n6MhdwJC5DrXScRRTwEWHURbhYQ5Qvw/Fhro7XdKIzJ2M7DedVcJeQHNbHWw/pHgqaQhl495JUXoeTbx6616LpUMd+GCR2LF9jLqhuEFM7iMSADS7AQrR2bVTx2oToyxBP0mVCEY9EipQtWscBtUbdHishdAoJDwBSRu4liwF4v1kcC/Rs0J9fwelzbcRw3brDYkSU3Iz5JopYmqHYnb7VFzOSiiinOBPq7xq3FbIKmFlWfQAg5oRPb1v8cO85XYgmWiN0XAWtHGT4+lDCsX/7xnRfWxUYnhy9CfHSx+zEW6ANz7BIfnRKRIiwpQBfuer7+Ejzf63FtN0ocr5uWh/OquUpI1ZM1ZX21ZvMgQfN1IiHNXXJJXslvPeXIf9UjkHqlkJO4KCRhPkKAdnQBvASoIBZHQhuRgdgZ5UhLcjpZt8IVMQtMb6q/Gi6vV7LyD3BxtcDfF7DLwHQxnF/NAVj6wNl0/iV4vtbCmlzbcSImYh/k0/LMnD+qibW67lHT0OapIc7MIcaVXzWteO86tGZqPhvqYZicXs6H08tZwY3syUWMaU8uboYTPB/Z85vhByyhMX0/O7sOnxb4fn6FMezZ1fAN+I5n305p0+fz06meCfl/9v884d+mssYfJ5cJ+sAPjz8Bzcn5+azGcMyaS/OF+BtrUG145no4Vs2sHc+Ch2LBjzhLkE+Oj3guOlR9OubDhB3HLmTHhUU30nd3usvZ+a17YXOB2tVc9IxN4YC02QQMrqTOw6qI9NUQLFTFRYGEixUhJIYknYIEE9jIhNDF/10cz5mOYiimxOi5BDJP5Dyqofi0rtk8nNq/DHTegs8Rv+A7/K4DdaE+Y72Ww+RqttYxMJ3xaIijLfwYxxGCo4XA8RE2cPEPUMC0gQispC28Eu9+LS5Ohzm6McYixuScYwx7JDg7/As4FKbGcFzlADivlidy/qd6aIP8FILQOOwhZ+Z0xLPwsS7Wi/pN2rsL53R7fy36Oa73mKGzPt4BbVGBi4bgHEcyaYELvFts3T0qSH5YFZ044zifqSNBmHa+hYYVwep0+rPRsf67iDkx38dXjeG43hXOq+VJi7FrCD9PtlKzeWiwOOItOcS48lt5txahS45TL+o36T9Z8UUaVr5esmH7BSasLi9chA0wQizO7giLJOA7KSfbd3iMaWs3XnCczwQTVbpZpDYscod8Hpxb16oxHLflkXm1PGkxdg3h9wZvjV84kG8RCnfkU7xWnuV3HZo2TSdYXmxW+KbR74S8Ouf3mnzefGDbzbWiiZ4LJVpAJCPxaUuC7h5CQjefhbvg2NUVJGzqY4ZAt7PDY2dT5L6zP4uY29fj2o4TMRH7IJ+WZ+b8UU2s1c0eHAR8qw9RkjfxGD8yHHGdOtSfAate1G/CH231U5i+Zcq3HT9Y+WEOtG8fTeICgBfPxQhfeVsyS0CJRpLxaQIUgTmWwChcnyYiIDs0CRFRJI6kudMT8f6gLSL/NmIn0a9rOo5iFpHaroGvPGkxVk18Tj9r9GdCEwdcULAulHkLDsmnv5sbz/SLDu17s+i1ud8NfwMsS4eUE7v2jgAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
            </state>
          </states>
        </style>
        <style role="TabControlClientAreaHorizontal">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabControlCloseButton">
          <states>
            <state name="Normal" backColor="Transparent" borderColor="Transparent" imageBackgroundStyle="Centered" foregroundAlpha="Transparent" backGradientStyle="None" backHatchStyle="None">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAEgEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA69AAAOvQFH+5CtAAAAe0lEQVQ4T6XKuw5AUBCEYY9OQ6KgcEkkEhIFhULh/UZOMQl7prAUXzH/bgLgNxm9ZPSS0UtGryhkaQp604PHoLwoQWpbUaCyamHZH5KR6m4C2dudjNSMG8je7mQM+nmHZX9IxmE5QGpbURjXE/SmB4/xlYxeMnrJ6IPkAg/MVpFoCuE9AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
            </state>
            <state name="HotTracked" imageBackgroundStyle="Centered">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAMQIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA69AAAOvQFH+5CtAAABmklEQVQ4T52T2UpCURiFz5vo00QEIgRlWZINF5n1EJUNZKVZNpgzcsSc0QZtUAuKLrxogKAeZrXXLg+dsovjxedm///61kEORxl2LpgFJQF6gJ6ZJY1l7z7aL++GoUdflMzjof2KfOXKMPToKzbx07xvI1uuG4YefVHiRr35gEypZhh69GVJpdZCungh6bPYNDqz/+b0vkom5sT/q0MtnGv0W+0a3e4d6NGXJWquilTuVMfAoOMPvzP0ZMnIhAvewDEWN0N/sAw5Nbrt6dGXJcFwSlssbR3D44tiJRCH1TatwTvn3Hey9L5LZhFT83K47ItgdTeB9WBKV9CBc+6ZY54efVmSKZ7B449ibS+JjUNVJ/rjBd2de+aYpydLRp0u8aoacuE9SuuEPbWKYPpUnj/nzDFPj75in3Th+u5RPmE7kkMgWZbiwUkNR7lLhPJX8uSdc+6ZY54efWVsak58A8/YCp9gJ1GSwcNsHeHSDaKVFmLVW3nyzjn3zDFPj74yPu1urG8H8fT2YRh69BXHjNskKArQA8Jzmz4BXxIC8qAC7kQAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
            <state name="Pressed">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAagIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA69AAAOvQFH+5CtAAAB00lEQVQ4T52QvWtaYRjFRXpjYmy118ZSaEyIVpH4rYiCEVR0UZwc1MFNFwf/hXRpIEM79AMKgRY6tEOhGbrnTzu9vxf6YrhTMhwenvP8znm5N+C67qmnW096hMidUnKXyWQ0HA4fLHLkKdFisVA+n1elUlGz2VS329V4PNZsNtN6vTaTHZ87HDw58qZku92qVqup0Wio3W6r3++b0HQ61Xw+N5MdnzscPDlbstlsVC6XVa/X1Wq11Ol0NBgMNBqNNJlMzGTH5w4HT86WrFYrlUolVatVvb28tOr1euZ15q4PB0/OliyXS+VyOfOdxWJR766urXh5d+cOB0/OlvDjUqmU0um0stmsAa7ff/IJnzscPDlbwk8KBoNyHEeHhxFFY8/lxl/ow+dvVuz43OHgyd0rCQQCcvZC2j8IK/Isplj8SB9vflmx43OHg/eVPHH25IT2FY5EFY0n9OX7b5/wucPB+0u8dlNw9Epff9xapQrNezt3OHhfSSjs/Qs3oZuff63OG33lmwMzd304eF9J+GlM7stjHb8pKFu9UKE1VPliZMWOzx0O3lcSicaVeH2ms/O6eXm34L/wucPB75bcJZNJYzxU5MhTcuLpD42PkJdzT/4B8f3Wrp0B9ZwAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="TabControlTabCloseButton">
          <states>
            <state name="Normal" imageBackgroundStyle="Centered" foregroundAlpha="Transparent">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAEgEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA68AAAOvAGVvHJJAAAAe0lEQVQ4T6XKuw5AUBCEYY9OQ6KgcEkkEhIFhULh/UZOMQl7prAUXzH/bgLgNxm9ZPSS0UtGryhkaQp604PHoLwoQWpbUaCyamHZH5KR6m4C2dudjNSMG8je7mQM+nmHZX9IxmE5QGpbURjXE/SmB4/xlYxeMnrJ6IPkAg/MVpFoCuE9AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
            </state>
            <state name="HotTracked" backColor="Transparent" borderColor="Transparent" imageBackgroundStyle="Stretched" foregroundAlpha="Transparent" backGradientStyle="None" backHatchStyle="None">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAMQIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA68AAAOvAGVvHJJAAABmklEQVQ4T52T2UpCURiFz5vo00QEIgRlWZINF5n1EJUNZKVZNpgzcsSc0QZtUAuKLrxogKAeZrXXLg+dsovjxedm///61kEORxl2LpgFJQF6gJ6ZJY1l7z7aL++GoUdflMzjof2KfOXKMPToKzbx07xvI1uuG4YefVHiRr35gEypZhh69GVJpdZCungh6bPYNDqz/+b0vkom5sT/q0MtnGv0W+0a3e4d6NGXJWquilTuVMfAoOMPvzP0ZMnIhAvewDEWN0N/sAw5Nbrt6dGXJcFwSlssbR3D44tiJRCH1TatwTvn3Hey9L5LZhFT83K47ItgdTeB9WBKV9CBc+6ZY54efVmSKZ7B449ibS+JjUNVJ/rjBd2de+aYpydLRp0u8aoacuE9SuuEPbWKYPpUnj/nzDFPj75in3Th+u5RPmE7kkMgWZbiwUkNR7lLhPJX8uSdc+6ZY54efWVsak58A8/YCp9gJ1GSwcNsHeHSDaKVFmLVW3nyzjn3zDFPj74yPu1urG8H8fT2YRh69BXHjNskKArQA8Jzmz4BXxIC8qAC7kQAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
            <state name="Pressed" backColor="Transparent" borderColor="Transparent" imageBackgroundStyle="Centered" backGradientStyle="None" backHatchStyle="None">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAagIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA68AAAOvAGVvHJJAAAB00lEQVQ4T52QvWtaYRjFRXpjYmy118ZSaEyIVpH4rYiCEVR0UZwc1MFNFwf/hXRpIEM79AMKgRY6tEOhGbrnTzu9vxf6YrhTMhwenvP8znm5N+C67qmnW096hMidUnKXyWQ0HA4fLHLkKdFisVA+n1elUlGz2VS329V4PNZsNtN6vTaTHZ87HDw58qZku92qVqup0Wio3W6r3++b0HQ61Xw+N5MdnzscPDlbstlsVC6XVa/X1Wq11Ol0NBgMNBqNNJlMzGTH5w4HT86WrFYrlUolVatVvb28tOr1euZ15q4PB0/OliyXS+VyOfOdxWJR766urXh5d+cOB0/OlvDjUqmU0um0stmsAa7ff/IJnzscPDlbwk8KBoNyHEeHhxFFY8/lxl/ow+dvVuz43OHgyd0rCQQCcvZC2j8IK/Isplj8SB9vflmx43OHg/eVPHH25IT2FY5EFY0n9OX7b5/wucPB+0u8dlNw9Epff9xapQrNezt3OHhfSSjs/Qs3oZuff63OG33lmwMzd304eF9J+GlM7stjHb8pKFu9UKE1VPliZMWOzx0O3lcSicaVeH2ms/O6eXm34L/wucPB75bcJZNJYzxU5MhTcuLpD42PkJdzT/4B8f3Wrp0B9ZwAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="TabControlTabItemHorizontalTop">
          <states>
            <state name="Normal" fontBold="True" />
            <state name="Selected" backColor2="White" backGradientStyle="GlassBottom37" />
          </states>
        </style>
        <style role="TabItemAreaHorizontalTop">
          <states>
            <state name="Normal" backColor="97, 97, 97" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemAreaVertical">
          <states>
            <state name="Normal" backColor="97, 97, 97" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ToolbarItemFontList">
          <states>
            <state name="Normal" foreColor="White" />
          </states>
        </style>
        <style role="ToolbarItemLabel">
          <states>
            <state name="Normal" foreColor="Office2007.RibbonCaptionAreaBackColorGradientLight" />
          </states>
        </style>
        <style role="ToolbarItemStateButton">
          <states>
            <state name="Normal" foreColor="White" />
          </states>
        </style>
        <style role="TreeCell">
          <states>
            <state name="Normal" borderColor="195, 195, 195" />
          </states>
        </style>
        <style role="TreeColumnHeader">
          <states>
            <state name="Normal">
              <resources>
                <name>GridColumnHeaderOffice2K7_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TreeNodeExpansionIndicator">
          <states>
            <state name="Normal">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAACQEAAAKJUE5HDQoaCgAAAA1JSERSAAAACwAAAAsIBgAAAKmsdyYAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAcklEQVQoU4XQvQ2AIBCG4RvJMRzDkRyB0pIRKC0pLSktKek+OQx/hX4kb3LFkyMgAMRYh9LR2zXTU1dgOwmItQgE7Y4IeVjW7d3KoG84X8egvwbMYMP6kAq/sPMj/tmocMIMzphAd46YQFtx/mHRgQVAHvU+julnFiLbAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraButton" buttonStyle="FlatBorderless">
          <states>
            <state name="Normal">
              <resources>
                <name>Button</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Button_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraCalculator">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraCalculatorButton" buttonStyle="FlatBorderless">
          <states>
            <state name="Normal">
              <resources>
                <name>Button</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Button_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonBase" buttonStyle="FlatBorderless">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraCalculatorEditArea" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraDropDownButtonMainButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Button</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraDropDownButtonSplitButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Button</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraGroupBoxContentArea">
          <states>
            <state name="Normal" backColor="229, 235, 235" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraPanel">
          <states>
            <state name="Normal" backColor="Office2007.GroupBoxContentAreaGradientLight" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraProgressBar" borderStyle="Rounded1">
          <states>
            <state name="Normal" borderColor="Office2007.OutlookNavPaneBorder" />
          </states>
        </style>
        <style role="UltraStatusBar">
          <states>
            <state name="Normal" foreColor="White" />
          </states>
        </style>
        <style role="UnpinnedTabAreaTop">
          <states>
            <state name="Normal" backColor="97, 97, 97" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ValueListItem">
          <states>
            <state name="Selected" backColor="Office2007.AddRemoveHighlight" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
      </styles>
      <sharedObjects>
        <sharedObject name="ScrollBar">
          <properties>
            <property name="MinimumThumbHeight" colorCategory="{Default}">30</property>
            <property name="MinimumThumbWidth" colorCategory="{Default}">30</property>
          </properties>
        </sharedObject>
      </sharedObjects>
    </styleSet>
  </styleSets>
  <resources>
    <resource name="BackgroundAndBorder" backColor="232, 232, 232" borderColor="137, 137, 137" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Button" foreColor="75, 75, 75" borderColor="Transparent" fontName="Segoe UI" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAADAIAAAKJUE5HDQoaCgAAAA1JSERSAAAAGAAAABYIBgAAANp9XIgAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABdUlEQVRIS62Vu08CQRCH58/WxEchmgAmYAw+AglgBBIMMSgWdpbGRAsRkTfccXB3HC+1/zmzBRUVTvE1u8k3OzM7uwSAiveP2ITr8sOKdfviply5gqfnF/RGI1XEKW5Kl24xnEzw2e2p0hk6EDcl80V4wRSN/kCVwdiFuCmRycGfzdEYWKrYvg9xUyyVRrBYomnZqownAcRN4bMLzJbfaFlDVdzpDOKmg5NTLH5+0eZFTaZcFXHTXjSGJQdYd4r/EMwXEDftHEURcJPfGy1VHM+HuGkrFDENeavVVek7I4ibA4RNvapfTVVcPrS4afswYgatymlpYvGgiZt2w8fw+UrVWh1VhmMP4qb947gJUG93VbFdD+KmEN9VKVG901PF5hKJm6KJS3icQaPHj5QiFmcgboonMyZAe2CrYnMPxE3n2bwJ0LEdVSwOIG5KFW7Mc913xqrYrg9xU5Z/ndfqhxk2TcQpbvPpF+4q2ISrUnnFun0A9AeYijQENa/dIAAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Button_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAIgIAAAKJUE5HDQoaCgAAAA1JSERSAAAAGAAAABYIBgAAANp9XIgAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABi0lEQVRIS63V3StDcRzH8e+fhwuuEFG4EsWd3ZCHKcmKGoULlMdRI09NHkqWiyFb6ySLNCzLQwtJZtvH53tWrlzpe/G+OOdXr9Ov07evABDf6jX+U/9i4re/ztWWgeVLBMNJ4Dttmppqi3chjkI2VezTsYue2tI1c47C1y0KH1HbaKotnskIv3iD/FvENDXVljZ/GPmPBHKZI9PUVFuaffvIvzv4ft5ju0btuaba0jQQQu41imx6g60bteGaakudd4tXiiCbCrAlowKuqbbUdK8Bt8NAtJSVGEWLptpS2bXKhxG+LCsemESLptpS2RkE7vxArIKVG0WLptpSpTe4Hwfi1azKKFo01ZZa/QepKcBpZA1G0aKpttR7N4GHOeCilbUYRYum2pyDbSC9AiQ8rMMoWjTVdicZj+vAVR/rNYoWTXeS2/2HwFMIuPGxIaNo0VRbOiaOgZcDIDnGRo2iRVNt7oMTIBMulpq2i57a4p2PYeeMa+7r1DQ11XaX/mDAwX/qmY399tc5APkBNSL4L2duZpMAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="DesktopAlertDropDownButton_HotTracked" backColor="255, 231, 162" foreColor="Black" borderColor="255, 189, 105" backGradientStyle="None" backHatchStyle="None" />
    <resource name="gradient_bg" backColor="White" borderColor="Transparent" backColor2="Gray" backGradientStyle="Vertical" borderColor2="Transparent" />
    <resource name="GridCellOffice2K7Black_EditMode" backColor="Transparent" imageBackgroundStyle="Stretched" backGradientStyle="None" backHatchStyle="None" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAKQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAGQAAAB8IBgAAABKwZn4AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAkklEQVRIS+2WMQoAIQwE8/9HeU8RtbMVsdKcKyLXBuJVGRAkxY6xWvLeU875CSGMeWetgzzkzjstQYyRW2s8xmAtkIdc5BOMGNyglLI2wndx732PdUEu8pfkJiYRYRIRJhFhEhEmEWESESYR8a9Eszl+Ob0LDa/Wuse6nAY5u6pLKa2B1kbYAA/fXdjhuy63ek8vMeXg0hJsTywAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="GridColumnHeaderOffice2K7_HotTracked" backColor="Transparent" borderColor="242, 149, 54" imageBackgroundStyle="Stretched" backGradientStyle="None" backHatchStyle="None" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAHAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAPwAAABMIBgAAACcx1ZIAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAhUlEQVRYR+XDwQoBUQCF4fOms1AWykJZKAtloSyUhbKwmGcZQogJIY8xc2fujNXvQc5Xn37fFNdqPxtcq32vca3mtcK1mucS14qPBa4V73NcK+YzXKu+TXGt+jrBtarLGNeqziNcK5yGuFY4DnCtcOjjWuW+h2uVuy6uVWw7uFaRJXhO+AN3cS9xNzEj0AAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="GridColumnHeaderOffice2K7_Normal" backColor="246, 250, 251" foreColor="75, 75, 75" borderColor="195, 195, 195" imageBackgroundStyle="Stretched" fontBold="True" backColor2="210, 210, 210" backGradientStyle="Vertical" imageBackgroundStretchMargins="2, 2, 2, 2" />
    <resource name="GridRowOffice2K7Black_Active" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAATgEAAAKJUE5HDQoaCgAAAA1JSERSAAAAGQAAAB8IBgAAABKwZn4AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAt0lEQVRIS7XDQUeDAQDG8eebjRg7jOgQY4cxdoglitl76LTLLp26dOrSqRgRsVleXl6bTaUsJWJf5Pn3KZ4fP/FzT7r4uiNdfN6SLt5vSBev16SLzRXpYjUlXdQT0kV1Sboox6SLl3PSxeKUdPHQIF2eHZAuPzZJl59apMvPbdLl+SHp8vKIdLk8Jl2uOqTLdZd0ed0jXd72SZffBqTLHyeky7sh6fL3Geny7wXp8t+IdHlfkF3wDxdbsa+UsdnsAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="GridRowOffice2K7Black_Selected" foreColor="Black" imageBackgroundStyle="Stretched">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAADwEAAAKJUE5HDQoaCgAAAA1JSERSAAAACwAAABQIBgAAAFssx2gAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAeElEQVQ4T5XTOwrCUABE0dn/gkSwsLCxsRBsBAsLQTAx/kjEz8v1LeFaHJhimikmlAErfDus8GmxMr5PWBlfR6yU5wErZdjXoU5Kv8MK7QTrz/J5ilXLsxqc0M2xwmWBFa5LrHBbYYX7Gis8Nlih32LVWzX1MkbDDzBIPq6579cQAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="ProgressBarFill_Normal" backColor="Transparent" imageBackgroundStyle="Stretched" backGradientStyle="None" backHatchStyle="None" imageBackgroundStretchMargins="0, 1, 0, 1">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAIgEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABcIBgAAAAL2z9kAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAi0lEQVQ4T6XUzQnCQAAF4VdDdvFgYkspRQRFVETxB8nBsgQRgiBINoogVvLcHubwwVQwGhxqE2r6swkdU2NCu+5kQptub0KrbmtCi7Q2oVlamtCkn5vQ+DU1oeIyNKHimgNQcStNKLSlCYV7DkDhkQNQeFYmFFIOQPFdmVD8jEwofnMAir8cALxy7T+o9lIecE4oCAAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="ScrollBarArrowDownOffice2k7Black_HotTracked" imageBackgroundStyle="Centered">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAALgIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABl0lEQVQ4T7XTTUsCQRzH8d5Jb6GI0B4u3YJCOiddFoKk8KbQqYPXKCi6dYioqENF0CXKskwsKoSkB+nB1M3VdXc1K912y18zY0yEiBZ0+FwW5rvDzH8aBEFAU3c/2pzTaBtdhtW9AItrvqaOkUm09g6ArmcRy/AUImIGumHC/ADeqVKZ8VHiTOKdIt/DcQ3tzpnvSOfYBlQtB3/sCUGpiFDGwIVm4pLwJnQuIL3hXDFwmzNBWmzdj0hazsB3pyIgvuBUNhBWTGYrppfFdfiTdUR2b2T4H55wJOk4IyFq/b7IbZMQ/cEV2eH/RryRFDweT01h1WCHWzVyEM3C7XZXFfw63JqR4GMBDoejwuZD8XeR47QBu93OrdwU/hahbDYbC9Qd2bmW+KzQoaOWrvPc6l0BPrF8c/8fobNyEM3hMJ5HIPGM+fMMt3iZY/OyR0L0HVWNeOlubhXsRzVm9kTk5kIpFlojO6qINA+OIxSJsRAlKyoULcvEEo+cmExBVrPIvxYrX3GjtQctQxOsXK8u1ywsfQKJCPgEdeR8VVSn3f0AAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="ScrollBarArrowDownOffice2k7Black_Normal" backColor="Transparent" foreColor="Transparent" imageBackgroundStyle="Centered" backGradientStyle="None" backHatchStyle="None">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA/wAAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAaElEQVQ4T2NISkr6r6en99/Kyuq/hYXFf0tLS4LY1tb2v6en538zM7P/IP2jhtDLkFOnThHERLlk27ZtODFRLoE5edGiRRgYJkdSmPT29sIxsjjJAVtZWYkhRrIh2PCoIZiYBoYk/QcAa/YV+PPV+JYAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="ScrollBarArrowDownOffice2k7Black_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAATAIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABtUlEQVQ4T7XUuy9DYRjHcX+DxG2xWTr4CwxikYg0kRhIty5ScYuhwmDQiURCwiJCFYlrGNrBJRH3KNpUL0ovSou2ip6eozeSn+e8kiMNjZIYPsubvN/hvM9z8pRKJQrLK1FS24nixoGclTX0QlZVD/E+i4gBveUapnAclqc0nNwrXLEPB+EUc0iOIynYntNw0/m61Y/Suq7PiFje90Uwdf6EaTeP1ZsE1m6TzJCDZ4YdAiZcL9D7E9gLpRB4eWP3MiI77ntMWkPQXXCYu4pDH0jCQPptPDNAxi6FnyMTlntoHY+Y8QhY8sWZHhMnGbTzWKCzzbskfMJ/RsbNt9BoND8yBBLw8q/ZI1r7A9RqdVZTbvq4uUSmXTGoVKov+iyx30VmvXEoFApJuzH6t4hILpezQEaEntlDA/d9xBSQZkUcOlHrYUSiPuEw6hSwSC90SVP9v5GPWaEVcD6TKJq37yQt+2F007yMUOiMdixrZNxMIWsQWluYadrwSlRbfhbqOuXYQmZECqrbsHx0wUKiXW+IFvKB0Z1eSWbMN5h3BLHiiUJr9GVucb6sAkU1Haycq8//iRLv2D8+utz9RdYAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="ScrollBarArrowLeftOffice2k7Black_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAARAIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABrUlEQVQ4T63UzUsbQRzG8fxFCgq+4U2oIN7Fg5d4EKxIL7XkpODBU0DwBfGNNlXpRUtBcim2UZFojGgbFWOMWeMmxo0b41uSZpM+nXkkYo1vQQ+f43z5zczumMxmM17KVFTbiLLWXlRavlBFxwSVv7c9qbS5B8VvGmCqah+AV40ilTbIyIIyOX+B7F24pmgXqH43CFN15zfosTgWD87IeZSk9WiatmIGfPFrhxcZOk5kSfQg1zNyrEXh8Ou0pF6SW0uT58SAxWKB9zmRHz6NFhUxkbB8lCKr1crIpm4gcJ6h8FWW8iJz3ggtBE7JGUrAZrPdRDZO0tg7M0i9zJA8l0cjdrs9PyK2JOW29V/E5w9gcGGXxtwhuFyuvMisIg/8D/0WQUne3IMR6VUik9vxwiP9jh0aWQnSZ49Ouci0PwGHmqI1cfXSg5GhJT+Nu8P06ZfGiJzs636SfoqQZIjP997IwLyXhp0Kja6q+LgeIRmSZsRU0k2kpMWK764thm7bV4J0cBiCGo6Qpp/S+VWSPMEYaj6MiUhdE8rb+lgsVH3XFN52ix/wvvehMGb8A10LgMwu7dRVAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="ScrollBarArrowLeftOffice2k7Black_Normal" foreColor="Transparent" imageBackgroundStyle="Centered">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAACAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAcUlEQVQ4T63UoQ0AIRBE0S2IFgg4GkCitgsqQNEERVAETc1lBY6ESwbxzIgvR1QVLHHOgSXee7AkhACWxBhxU2s97ts10nvnImMMLjLn5CJrLT5inkTMk4j5FUkp4cYip32TnDNYUkoBS1prYD34E8UHNqUyXTWpPTIAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="ScrollBarArrowLeftOffice2k7Black_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAARwIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABsElEQVQ4T63Uuy9DYRjH8fNXYCAxEJvYRcwGIjaL4cSdxOLWSWIhpBubUIeBGNyqCEJcIk4oRSlO9aCNtqpxil6UPN7311TcGhGGz/rN87zveY8giiL9lZCUnU/JBQ2UUtr1a6lFjZSRW0hCWnEzGS0XZPYGweJ/ApsWhbNAlBz3Mc7HZ/CEXsCs3lB6SSsJvLih+mjw2A9Dyj1MXIZg3hWmVU8E9licO2Vxzh95wUSIrCrXNHDgAelEgxFHEIzOMOl0Olpxh3+O9FtiDEe3MGx/AL1ej8gsm0j2PYH1Lgq+8KdI364LDNYbGDoLkCRJb5FJtto6W4mLn5v3p4jJZEoYia/1IdK/eULitBWqFh0ky/KXSLslQIPKIxidIThnN5Ywwv1LpH7DmzhyxSKMnX1DHyOTB1A5r0DdmhvikeZtjXptDzCmBoFf87eR8hkbVC+pULviQoRPpjNr0MNC3D473G8j4tQhVMydQtXCOdUsXwEPcS07Gmx6I7EIf3zd01sIvWeQFZB2HDS8ewmjR24Yt9+BQVYpq7SNhMy8IrxGXvytHLGTypo66B/+JyK9AruqQ3sbClyaAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="ScrollBarArrowRightOffice2k7Black_HotTracked" imageBackgroundStyle="Centered">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAARQIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABrklEQVQ4T6XUzUsbQRzG8f2LKlhQW7wJFaR36aGX9FBoi/SiJScFD54CghpKW0u7VPFSpVByKWpURIyR1JcUt2nMGtc0bkyM8SVxX/p05glbfCsaPHyO82Vmfruj+Hw+3JZyp/URGp71455/jJq6PlFjp3qtu0/6UPegHcr9jiFoRg4VyybbBTmeP4B7Eap08xDNL4NQmru/IF8oYm7rgBZ+lymWsyhesJEoVm0fOrR74pLoQa5nZNfMIZzM07xxRJFIBFHTwtqeDU0EpGsjUwmT5nSxI0FGQqEQlkVoPW9TquRQ5tilS5FJLUuzqX3yIqqq4vueRb8ObDKOHJL3cuNIIBCoRsSRJO9Y5yKJZArB2Z80HN2hixG/3y8u/JRWRVCSk6sp8lWXU7tFRAZuFBkMb9DbxTR5kdGNEj4nTyhsVEhOTPpv5PV8kmTkfTSDjysmRn4UaWKzTNMiJNni870yMjSj0ZsFnd4tGfgQy5IXGxe7kv5F6p8G8C0SZ+isTT1NW9s7MDJZMvP7VDou01q6gJZXwyLS9hiNLwZYrNXDnlE87xU/4FXvQ218+AsAsIq/CNwjlQAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="ScrollBarArrowRightOffice2k7Black_Normal" foreColor="Transparent" imageBackgroundStyle="Centered">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAGQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAgklEQVQ4T73UsQkFIRAE0CnIFkQzGzA0sgsrMLIJi7AIm5rjgg/HMfw72M8PXrKzDBstaq20gnOOVvDe0wohBFohxkhl7y3nyteStZbM7h5L5pwyv3pVMsaQOx+vS1prcu/0v0tUfvVYorI7pJSonCVqriDnTCuUUmiF3jutfvBPKg9gFzvhqbTMWwAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="ScrollBarArrowRightOffice2k7Black_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAARgIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABr0lEQVQ4T63Uyy8DURTH8fkrsCCxIHZiL2JtQcTOxmLiHWLj1ZXEhpDu2Ak1LAiJVxVBSKNpTChFKYYO2tCqxnj0oeS499dMoiKRhsVn+80998wdQRRF+ishLb+Y0ktaKKOyL2WZZa2UU1hKQlZ5O5mdV+QIhMEZegO3Fofzpzh5nhO8r+/gj3yAQ72n7IpOEnjRpgZp9CQEY8ozzF5HYMUXJas/Bvsszp2xOBeKfeBEiFiVWxo59IN0qoEsyzThCZPZG6XNu4RfI8POBNPxA/CIxWKhaTVMS+w0nBx8A9djHILRb5GhPR+YXPegRyRJojk2FrfFRuL0ewukEjEajUkRfaykyLD9lMQFF9SteeB7xGAw0KjyCmZvBC7ZxlKKdDuf/hbhgaTIDYswF+wbSo7MHULtigJ6pNkepPYdDQbdLzDFNsbxNf8YqV50A4/Ur6vUuOmjJlsADA4NBliIO2CX+2NEnD+CmuUzqFu9pIaNG9BjHbsa2AOxRIQ/vv6FbYS+MskKSLseGt+7hsnjO5i5eASTrFJeZRcJuUVleI28mKoCsZeq2nroH/4nIn0C0ilNjFaSZfoAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="ScrollBarArrowUpOffice2k7Black_HotTracked" imageBackgroundStyle="Centered">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAMQIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABmklEQVQ4T7XTTUsCQRzH8d5Jb6GI0B4u3YJCOiddFoKk8JbQqYPXKCi6eQipqENF0CXKnkwsKgSlB+nBpy1t3V3NSrdd89fMGBMh5Xbo8LkszHeHmf80CIKApu5+tDln0Da2BKtrHpZRb10dI1No7R0AXc8iluFpRMUsNN2A8Q6UqUqV/l7hDKJMke+RpIp25+xXpHN8HYqahz/xhGC6hFBWx7lq4ILwpTQukH5DWNZxkzdAWmzdt8ijlMXerYKA+IJTSUdENpjNhFaV1OB/MBHZuZbgjz/hKK3hjISotbsSt0VC9AeXZIf/G/FFMziI5RC8L+L4UWfcbjeWr4vMRryEYOYNEUVnh2sq4nK5OB75PFxTEYfDUeNPEbvd/iNTEZvNVtevke2rNJ8VOnTU4lWBW7ktYk+s3tz/R+isHMTyOEwWEEg9wxvOcgsXeTYvuyRE39GPER/dzY2M/ZjKeE5Ebi6UYaFVsqOaSPPgBELRBAtRkqxAVnNMInXPiQ8ZSEoOhddS7StutPagZWiSlc3qGvXA0ieQiIAP16d2GmENp50AAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="ScrollBarArrowUpOffice2k7Black_Normal" foreColor="Transparent" imageBackgroundStyle="Centered">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA+gAAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAY0lEQVQ4T2NISkr6r6ur+9/W1haM7e3t/zs4OODF7u7u/0NCQv6bmZn9B+kfNWSgDDl16hQKH4RJMmTbtm1wTJYhixYtwsAkGdLb24sTE2VIZWUlQUxywGLDo4ZgYhoYkvQfAAMeHFjoz7unAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="ScrollBarArrowUpOffice2k7Black_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAATAIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABtUlEQVQ4T7XUOUsDQRjGcT+D4NXY2Vj4CSzERhAJCBZKujQS8cIiooWFqRQEBW0kxHiBJ1qshQeINx5JiIlrzG0Sj8QYs9nNqfA4O4HVoOJaWPyagfnDMu+7BSqVCsVVNShr6EFpy7BsFc0DqKxtgnifRsQAY7mFKZyEJZqFnXuFM55zHM5QJ8R5JAPbSxYucr5lDaC8sfcjIpaPfBFMX0cx6+Kx7k9h8y5NjbI8NcYK0DsTYAIpHIYyCCbe6L28yL7rAVPWEGZuOCx4k2CCaWwQQzaeGiYmHcLvEb3lAQb2GXNuASu+JNVv4iQjVzyWyNnOfRo+4T8jOvMdDFdPmHXGMe9JUlqtFl1nMWrQEse0K0E+MwUP/yovotFoJJ8jjNyIWq3+4k8RpVL5I1kRhULxK/GZ3WTgvo+YgtKsiEMn6jiJSDQXHCbsApbJCznIVP9vJDcrZAXsL0QMbXv3kvajMPrIvIyT0CXZsR8jOjMJWR9hsIWp1m2PRL0boKFeI0cXMi9SVNeJ1dMbGhIdeEJkIZ+oGaNXMmf2Y5F9xJo7BsOZL3+LCyurUVLfTctyffxPVHgHpGM4fvQiSWUAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="ScrollBarThumbHorizontalOffice2k7Black_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAABAIAAAKJUE5HDQoaCgAAAA1JSERSAAAAHQAAABEIBgAAACFRp3QAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABbUlEQVRIS93Vy0oCURzH8XmXnsBapNkDRNALBG2joMuqoFUvUAQhswgiCKNa2qJNF0RBLUKJUKZGHQWHGUdrvOSlGadfc0ZCgvHQQFPQ4rP+/v//xTkMgF/HjE7Pwbu0i8nNkOtIh/QY/yqLnKyiZwC9d8Bwg7keUVCamFgJgCETqPUG4mIbqYoGTtXB13RkKYRGz5HSqwGpZVhh38ZJP1pWKogIKhJSF/dVDZkXnYoM5USx2Q+TqHc9OIhePcqIFuq4KWtIKnQPz7ojJEw2JqceXzscRC85CeFcFTGxZYVp7AahIdfJ1vU/iJrbfonyOQGBMIe9RBHBtIpTvk0VynccuS51EZfeoJvV/x+NDYuyUR4HSdkK09gNQnNe6CAsdu2jn2GyMQ0ZzIljrokzoQNtWPQ72MiTI/t3Io4yNWjmO2tFxxZ2cHGbtsJuEmUFqXwZ3sUtMCP+GXjmt21/hZ/mWw7AMzVr/9+5C8wHr3xgWssIgI8AAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="ScrollBarThumbHorizontalOffice2k7Black_Normal" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAzwAAAAKJUE5HDQoaCgAAAA1JSERSAAAACAAAAAgIBgAAAMQPvosAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAOElEQVQoU2P4//8/Q27tBCCFnQYryKudBBbARhOnICy/HyyAjaaSgvgSiKux0WAFOVBHYdKT/gMAioOsuZaDetEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAAgIAAAKJUE5HDQoaCgAAAA1JSERSAAAAHQAAABEIBgAAACFRp3QAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABa0lEQVRIS72VS2vCQBSF84sUUUQFV678wdKNbX3bSDRqJE8bqZWKMYnGt25OZyY0QumuExff+rvn3MuMAODhCIlEAslk8mFQn5DL5aDrOi6XC67Xa2zcbjeYpolCoRBK9/s9drs9jscj4fRvTqffnHE+h6GoT8jn89hstvB9H0EQsAF4cziEYWib1MeknufDcdbwiJgOwJufMDRtJHVdD6uVg/XaZQPwJhTviPR8l1LZcrlkYsdxuUPF223A9htJbXuGyUSFblgwrXfuzGYfWCy+WOJIOp3aGA4VItagqgZ3LMvGfP5JjnVzl5rWFFJfZuKxonJH002WltYcSQ3DhChKkCQZg8GYO7RB2iY92EiqaQaarS463R7eiJw3Yq8PhSSmh8qkxWKR9K7h5bWBWq2Jer3NnUazg1ZbJHtdgPqEUqmESuUJsjyOjdFIYUmr1WeUy2UI9OVPpVLIZDKxk06nkc1m//7v4gXCN0dqK+nnbEvEAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="ScrollBarThumbHorizontalOffice2k7Black_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA9gEAAAKJUE5HDQoaCgAAAA1JSERSAAAAHQAAABEIBgAAACFRp3QAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABX0lEQVRIS93VvS9DURjH8ftXNAbC0kXiP5CITQwVDGIlwtCUgbAwmZhILCJNpdhIvCTidahWaShaWrSo27fcVqu9uW1vezU/9xwxiNrOJTF858/znHOSwwH49Th9owHVhmFUdU9pHnGIx9W2j2LbF8N1RkFAVBCS3hDOsU3Il5GQyzgPJVHXMQaOTODk07AGRKzxBRzEZRwJRdgZ5nlV4M8qFCYeRW3BOCw3SSw9SNgIF7Abk7HHsLOXEoXJxl/QhYsoLL4Ulh/zWH1mm03d1qXC0W+oOwKzV4D1PkthlpFtyTGT+/1b1Oy8Q8+6F307ARjtAgZdGabN+iUsBnPwpEv/HJ1RUctPaO+WDwOHPIVZNukRMXcrwZ2qgH7CZGOWmU5SmLgUcZwoVka1qH//CUZHgr7iD7R1CNObpxTWspWrCOYdQei7xsHpGpqgazHRCbSupm0E9c2dlf87bQP3DrJz5KokVtUiAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="ScrollBarThumbVerticalOffice2k7Black_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAABgIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAAB0IBgAAAEyvh4EAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABb0lEQVRIS+2Vy0oCURyH5116AmuRZg8QQS8QtI2CLquCVr1AEcTgIoggjGppizZdEAW1CCVCmRp1FBxmHK3xkpcc9dc5R0YtlYJWwSy+AwPn++YszuHPAeDGZxcwtcbDsXUG+6YbkxvHP2Jf3oFtZp7o4FjEvrKPlFZGi3ww2j0MsvTToDTbiCSzcKzyvcj0tofJSqWFzHsLUqmJeNFAvGAgqHwwAgSvXMeFVMNJrABZ1UC9gQgNpMtNiESOvXW4zdS7XKZqOBXKOHyQISakIRFydHoCGnh6NRDWGgxPstblXKziKKzC5XuxIlbEivRF6Kulj47Kd9kOVDRxR3UchNLgvYIV+Z8Rekf+HBF0A4/5jhyQKwwqmrCL5hdHR8goQSTXQEipw58qwpvIM+ifTczA6AiZGUG5Cp+k4+ZZxbWgMEzpOwMROgETqg69WEJWy32Bbh7G1X0UtsXdXoTOYue6i5V/y8TSHsacc0QH9wkF02BaKILIlwAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="ScrollBarThumbVerticalOffice2k7Black_Normal" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAvgAAAAKJUE5HDQoaCgAAAA1JSERSAAAACAAAAAgIBgAAAMQPvosAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAJ0lEQVQoU2PIrZ3wP692EhiH5ffDcXzJhP85QDGG///x4+Fgwn8GAPvVrLnFtzfQAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA8gEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAAB0IBgAAAEyvh4EAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABW0lEQVRIS+3VS2vCQBQF4PwiRZSggitX/mDpxra+bUSNGsljYiO1UlHzMEYTdXM6MyJFsJTuCs1iyOp+9wyBOQIAIZFIIJ/PI5vNIpfL/XgKhQJKpRJEUaTjEDiSTCZBCMHpdMLxeEQUsRMhDEN43hau68FxXNi2g9Vqjfl8gUrlEel0+hY5n898+HAIEQQBfH+H7dbHYvFBh94xm71hOrWgKCqaLYl/M5nMLcJShGGE/f5AAZ8nYNvZsGla0HSCyUSD1O2j3mhjNFK+Qy4prsB6bYOYr1BVA2O6eTAY40XqoVZrQZbH/wK5/pkYiZEYiZE/glxeesd1sdnYvGN0w+Sv/HCooNeT0e50Ua027iOstHa7gBYVS7DhwHK5pIB6AfoyJJqi0ezg6bl+H9F1/eYqrO1YGsua8dIi5hSGQaBpBr2ehnL5AalU6gthXcx6+DddXCwWwZYDED4BVVAr6SBgcPwAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="ScrollBarThumbVerticalOffice2k7Black_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAAAIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAAB0IBgAAAEyvh4EAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAABaUlEQVRIS+3VPS9DUQDG8fspGgNh6SLxDSRiE0MFg1iJMDRlICxMJiYSizRNpdhIvCTidahSGoqWFi2qr7mtVntz+3rbPO45cttUNWlidIf/dp7fOdthADDKVhWaeqZQ1z9Xc8q+aTS394pzMBSpV43j2hNBOFMAmyogIOZL5mm2WK6YNZrDeTiLw2AGy2duNHRNlBAiE8CZEGD7FGD5yMHEZmkr7iRNL7b0yGPmloP6LIz1Oz99URlCXkCAKxEwimNyG2nRydMWxGZtHDQXUQwfvUFnfqpEyNPJzWS48Z7C2ut3o5Z4MbWJxdC+CwNbdhmRERmRkf+EnIrIwV8QD5/HcSiDbV+ajg3PCRoZSo2ceDG466iOuDgBm940Vl946B1R6OwsjdwsJQFVkfu4AIOLg/4hAu1NAFqrnyaNflaBkB9wzxGE2RuD0R0qixz+rfmdSyg6NCWE/MWN3ZNUrrnOMSha2sQ5mC/p4eSqEciA7wAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="ScrollBarTrackHorizontalOffice2K7Black_Normal" imageBackgroundStyle="Stretched">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA4QAAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAABEIBgAAADttR/oAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAASklEQVQ4T+3UoRHAQAhE0S2IFhhwNHASRTOUvSkgcqNuIp798mNmqIKZUQV3pwoRQRUyk6o/8nZbpKqowjmHKnQ3Vdhdqj74yfABcWUanMuLfWsAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="ScrollBarTrackVerticalOffice2K7Black_Normal" borderColor="Transparent" imageBackgroundStyle="Stretched">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA5gAAAAKJUE5HDQoaCgAAAA1JSERSAAAAEQAAAAoIBgAAAFJ8taIAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAT0lEQVQoU+3NsQ3AMAhE0SxkSoSREO4QvSdg/x0ucpTSShZw8cr7d1UVWmsws4e7Y4zxKSIw5wQzY+1P5ER2thEigqpCRNB7/7XOMvONFG4hDC7CUVkpxwAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="spacer" backColor="Transparent" borderColor="Transparent" backGradientStyle="None" backHatchStyle="None">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAqAAAAAKJUE5HDQoaCgAAAA1JSERSAAAAAgAAAAIIBgAAAHK2DSQAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAAEUlEQVQYV2P4//8/GEMZ/xkAcaAL9S6Tn08AAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="TabControlTabItemHorizontalBottomOffice2k7Black_HotTracked" backColor="Transparent" borderColor="Transparent" imageBackgroundStyle="Stretched" backGradientStyle="None" backHatchStyle="None" imageBackgroundStretchMargins="5, 0, 5, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAFwUAAAKJUE5HDQoaCgAAAA1JSERSAAAAVgAAABYIBgAAAFoOa5sAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAEgElEQVRYR+3X6VMbdRzH8fxLdqY60xasUEXbkdIGhFLkaMno2LENEI4k5CIh2ZCQiyQkHAlQEC32QvvEHloCKbT1keM4OjqOjo5HW2sPfPrx+/vtxiTsthAqnnnwgsBufvv9vWezDKrTswI+SQ/hs7SAL5Ys3FeLXdw3qTe571Ia/LDYyP2UquNuLR7kbi9VcnfS+7hf0hXc3XQ57l17lru/XCpa2cE9kDy8/oxo5Wlu9eZ2rF7fJrrxFPfbx9sUrd6k40zmfPLwxnaRtN4DWptb2cndXxbdW6a5yK/LZbh7bQ93h2ZmbtMemFtLVdzPqRrux9Rh7vuFI/h24XXu61Q79+Winvs87cCnaR/OveOE6nzyKCa9L+FU6HlciO3GB6OlorFSXBwvkbmUKM1zmVxJluT5kJkowUdMcldWQvx+dWIXFibJScmUKDW5U2ZxaoeM0nmZNXJdpd/LSXMRPucaVxIluEz7zLgk7VuG9cnBms1Hy3jLuRkPVLPxY4j2V2JU2IdpfwVODe3h5si74XKZ05EynFnjLC2Y69yw6HyMeU5mnrwXJyMS9pq8H98tc2FETum8zBq55ulGkcvMlZ0z19loeZ4zhO05n9hiLkydJG/TjZn07kXYsR+zU0GoTsY64LceQMhWhfEBiut7McsvmglszltcxZMJKgi88ATY+0UzfwK2zrSvAknPXoT69vOW0xNhqJLDFgiGGnhN1YjaqzDmellO2AJK13FVbpDSexUoXXcLjNBM4b4qeHqrMUCmklGoEtF+9HXVQdC/goBZjbDtQFGBguaD8Bqr4aKGTjKZoLDjw26YO+rh7KmDl2r7zUWFUdOdWkNBa+HoroO9uxYT4xR2LOqBXtsAe1ctPOyRQOWLCsM+7axfX2cdbCTJwo5S2O7jDfyXbn0NBooK5qS71Karg1V3CJaOQ0iOUdh4hMKeaICFDjh76BlRVDB2t1ooqqm9Hr0kMUphh4cEdB1/FWZdPZ3AnhFFBaFmtk6KSn+nDG2H+WN1nIUNB5zoOtEEc2cT7D2NRZvQ191I/Rqhb2tCt7YJYyNSWH37EVh7WuEwFm2KoRU2vQYG3VF0aVswGqewQ34XjLrXYDe+gf7eos1yUD+r/hg6tRqMxCNQBX30D4JJC6dFC5e1aLOcxG5ug0mvxUiMwsbpi81iQJ/FCLt1LQPnsIncjp7/JYdN/0eDXJk+uc1YS9ZUFRuOwNVvk1jzuDOctsdgx60YcMl5MoS1LOvyugujtIacdP2c2ZTmZjL7Ut6zROqTbSZ2jIRD9CgI+OH1CBKXokE6lsV+dsHn3Ti/jPMvln99pRkfJbPfrEd3EgnwDXqhGhhwI+AffKxgIJd3Q0IKwsHBfwSl2ZT2oCzbQqkVIwguqCxmE70QEKbbd2goqCi8Cezj8G+gNPt6lBpluFxOmIxGqDQaDQKBALxeD6LRSMFiseh/ktJe18MaspYaTStUarUaLS0t6O3thd/v5yKRyJZQGubvoDTbRoVCIfh8Ps7tdnMOh4PT6XRobm6GWq3G71HFl6D3HF32AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="TabControlTabItemHorizontalBottomOffice2K7Black_Normal" backColor="97, 97, 97" foreColor="WhiteSmoke" borderColor="Transparent" backColorAlpha="Opaque" backGradientStyle="None" backHatchStyle="None" />
    <resource name="TabControlTabItemHorizontalBottomOffice2k7Black_Selected" foreColor="Black" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="6, 0, 5, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAiAMAAAKJUE5HDQoaCgAAAA1JSERSAAAANgAAABYIBgAAAIvpb+sAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAC8UlEQVRYR9XW21LaUBQGYN6RF/DG0YfxxtEH8AW8cqagTi1Yq+1Mq7UeQBBCCBAgJxIgAU9/104gJzYzbUNn4sU3jsOssP7svfYmc1ip4aui4Z7+/vj1gMsl7h7qKyCsxD1HqdJATZRwrZvIl6vIfDq/wLE1Ra3RfvfBWp0+zsdPOD49Q6ZQKCBvv0Boyri6qXD9vK24halRjSpXRVenq6LovODk5CQIJrZkXN9Vlyo/UnFCD/9DremqELnHgr16wYrFIo4opdTu4aZUc92W6wuqdSk5obUSjzFsjJieouPzJBTskFKy/Rmfg/tKoNZgD0gJkYLM1H0d9NVYsCMK1paVyB4uxwhUmFhz9RouGQ1JhqIZ+BIOdkz/dLoKZw/T/p1hhWkhSt1Aq4vmjKoPcD6NBevS4FXrTd8jzUTYvDgJqb0qPV+rE9ANExfzYOxU/OC8odfXgmHkkKgwqXATyfR9bUb26IZFwd6CYDkWjE6UOg2mS1zUosKk5g2sUoehMWKMASeYohqzQexAZKQomQpTg8YmjI0RMzCH8WCvULUBzYHsk9hMhHT7VJwSbGwi6LcuY1ojXDyFgn2k31fsRGnT/g3QUof0aaumhUL31SID1jAW7Gw4gUbBuMs+o9IdkVYaQ/0PR+NosO/m2P2g5y4zH/s8LXSDx8RoHAtWMobQqUBRtaVUlx6lLafF6eytGvQ9YbwGPUaESSdeYMBjmhiP7SAYu6CbVMgepumsgTmvkbBoUxxGWKzRQRynOYYajDNNK8ris20b3+YXdD6fR5f2qONM/s6EZ+qbxE2jpiv3hJHt4NJ5xsHBATI7OzsQRZGWcfyP7D/C3qbH4XKchOjlqfTCr+g82N7eRmZjYwNszgRBoLmhS462CT+Ahw3o3DDG8tmwKIQ5MyAGfSmjz9EK69SMFqKG0Sp46Cpa8ByhkBY9s9RXUDg9xfr6OjLZbBZra2vY2trC3t4e9vf33T363uRyOezu7mJzcxPZbBa/AaXg+PP7QeCEAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="TabControlTabItemHorizontalTopOffice2K7Black_HotTracked" backColor="Transparent" foreColor="White" imageBackgroundStyle="Stretched" backGradientStyle="None" backHatchStyle="None" imageBackgroundStretchMargins="4, 3, 4, 0">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAGAUAAAKJUE5HDQoaCgAAAA1JSERSAAAAVgAAABYIBgAAAFoOa5sAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAEgUlEQVRYR+3X+29TZRgH8PMvSaImMCaw6RQiDnZlTMZgjUYidKO7dL1f1p6uXW9rz9rduo3hlMlV+YU7dJQN9CdjjEZjNBoviAiKv3593rft2vW8c2slBLU/fNat7+lznueb97zNpLq6Ouzfvx86nQ5Op5PzeDyc3+/nwuEwotFoyRTl6SDq7XEIBAKc0WhEe3s7WKaSRtOBYDAIn88rbGYtsZjynySadS0sQ5alRqOBZDIY4Ha7MDwcWlWkBNEI7fJ/AVHvaxFlxESonizLsJhNkGTZjWBgSCgUzOdbl7BAJDT0VBD1JppBLJeFKKt8g4MeSP4hH3xembhXNUTrQ/w1x+9bv4CK6wlbeX9Rj6vJzcwySBNllMbWZAo/AIk9Du4BW4aV82S5bH+DrVsx6Bbz5pOzLGvyeYojqqGW3wPJ9CXqOzuXeOaMTD7ZvHLSOcZGopDisShsln44rIY87O9+OG0ienicff9L2Qyy+azMzAC7xcCzZJlKo/TDpNfCYe6Ey6qFu6xkLosWdpMWIf8ABRuPolurgVV/CE7DWxgwlpXKQfkZdG9gOOCGNBZX0KNtR7/uIGx6DZz9HRRwWSmsfR3Qdx1AJOiCND6qoFfbBn1nG8zd+2Dv3QdHX1kpzN1t6DnSlg52YkyBXtuK/s69MB1tga17Dxw9zXD0lhWFMjPrWtBz+HWMDMuQJilYY1cLTMSiY6E2wdXXWFYCi64ZvUdaEY96ISXGFViO7oGVQrXRgqu3CYP6hrIieYi9m4I93IoxhQU7odDj38zfZLtV1jfCZ6gvK5K3v4Hnx47VcRbsFAXroF3qpHPCpW+C19iAgLmO1JcVwWesp+OAzln6npoY8UCanlQo0Ea4Mzs1ZN6NiG1XWZGCtBnZ026nL7FJhf5BmEkoGKS0vSRir8WoeyfG5VefDPd6UU/rIvis6L7/lOA+iqMWPlM9ZDoSEiMWSLNTEQSsuxC2v4aEdztm/TV4J1iD448Bq5PzUulC9PlCqvrFepl6LM1sIMOfMzG4A2FbLc/yWOwopLmZECJOCtW3He+GX8R8pHrZ+5EqnIwy21Y4pVThdIEzI9tUzsbSznFbVD6Ib1X5ML5F5fyomui65TqjGfT7udhW6kEk1+dpZaVTbMY8bGaWRaH54WqcyJgN1GBM3gFlYCfm4ocgzR/3Ytr3Cs5RwQtjlbg4XmBis8olcjnPlcnNuJpQuzaVdn2a2aRyY6ZCJUnvF1qY2agium65zrGM6Qpcn6rAtQSZzLxmUV9XGerzSoHLk5W4VECUwwXKh2XGnKeNcoI2JsvybOIgpDPvufBpyo/PU058uaDnvk52cd/eeJP7/sYB/Jjcy/2cbODu3qzlfknt4O6larj7t6q53xa34cHiC9zDxU3c70sZt5/n/lh6Lu3Os9yj2xtyPnqG+/PjDUKP7tA6s3w91cjWY7XZPZY2cg+zFiu5B7eoL3I/VYVfqWfmHp+BZrm5k7u7sJv7KdnM/bCwj/suqcE3ybe5rxZ6uC9uWrjPUjI+SQ3j5JyMvwAduJegm4dnsgAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="TabControlTabItemHorizontalTopOffice2K7Black_Normal" backColor="97, 97, 97" foreColor="White" backGradientStyle="None" backHatchStyle="None" />
    <resource name="TabControlTabItemHorizontalTopOffice2K7Black_Selected" foreColor="Black" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="5, 4, 5, 0">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAggMAAAKJUE5HDQoaCgAAAA1JSERSAAAANgAAABYIBgAAAIvpb+sAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAC60lEQVRYR9XW3VLaUBAHcN7RF/DG0YfxhtEH8AW8cqagTi1Yq+1Mq239AEESwjfkOxBCVNzuHiDkJAHbIU7jxe/CcTY5/3N2T0itra3B1tYW7O7uQiaTgZOTk3dnf38f9vb2YHt7G9bX14EypTY2NiB3egqFngwNZwSy+wRKyGMEd2I0p/pozAg0fCbRp4zhEMwpi9jEZvoBAx97CcMwQFEUEEURcrkcbG5uQiqdTsNVTwEFX+rgYhzHWclwuMiQsTHI34kO8RpJkmBnZwdSBwcHcOk8wQBf7uJJuHgKcRoF4eb5sc300OYG0Ib/g46qQzabhRT16Dd3zHbTtKxoJs8wzTAjTGcMjqYH6XPacmokzUPPq+km5PP5SbCLxxc8xuErC5zCQh0fMBNe6JRGIhal4iJ8lGWUIBXkJeidBb3P5swLRgOq4aKjFqliQVJ08T5YhP7/3bT5YP2BPSnGHlUSrN2RF6L1n/VHfDCrP8Dj1JEW0pOTo9nqBXQ9Cgb7aD/ywUxrAF1ZnejxOnjMSVFvdDi1RtujqAZknLEvmPuCF0cfOl0sRu2giGP/X6R6i1drQXWKOi7jvPDBdMOCFhYybTziN9CIgSA1wqoTXWzVUDBNt+aLwN5tcLqxqMegIjZCHqZobD7wwcYYzOQX0fTDfo5Bjc3Eah6EOqcs1Dw0Rsej8fwDfY7B6EaZFUukPlettxPj/qHmI3FojLhgX/APGb8RrLhGWiC+BWl1xXKVUyCliWZbhiO8Fb1gnzFYD79XAhYKUpOp+OFgJsVdSfTc3vsJOEI9OAwGoxulUqUQTW8YZ8pxYTOxmpuiEHJdqDB0Hxw5z/NgeUxJ36tSBft2JtC/cSjG4NdteSEJP9LZ4fP8VsxjylZHwZ7FYsT61m/aw0nw86YEV9fRRLwfvGDHp2dwjr+vqD+p8A77NYjv5VUIK7v8fR/pB6IxOrZc+HR+AalsEY9RM/GSqL/7YHelCnzF37qHpQr8AT0/+PPd2XOwAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="TabControlTabItemVerticalLeftOffice2K7Black_HotTracked" backColor="Transparent" foreColor="WhiteSmoke" imageBackgroundStyle="Stretched" backGradientStyle="None" backHatchStyle="None" imageBackgroundStretchMargins="3, 2, 0, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAVgUAAAKJUE5HDQoaCgAAAA1JSERSAAAAFgAAAFYIBgAAAMvsZlcAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAEv0lEQVRYR+3Z609bZRwHcP4lTdRkA2SFwRwZYwMGY8hlo9GwuBUoLb33tKU9bQ89vR5a6AUYiA53Vd/oNjcKHWzzlVkWjcZoNF62Od3Ft1+fc1o4sAGu7TmNJL543n7y5Mlz+/2+ZU1NTVAqlbCYTfB43Aj4WUTHIohFIwj4RhBiXQj7ncIYC9GIRbyY4LyIk5EYcyPJjSA1ZsGZ6CDmYidxKXUC5+ZolPX09MDv94OmafjZUTBeGq4RCpRFD5tJBYP6HegGjkNzuguaU29De7oD2lMd0Kk6YB5sh03TBlrfAtZ6CNxIA6aYt/BVJoQyo9EIhvEiHA5ugG0WA5wWFeyGPliHe2Ee6oJZ3Q6Lug22oTbYNa1wDhNUdwSMqRlBqhETdD3OBvfiXobMmGVZcFwEoVAgB7sIbIXdaoDLqsKIsQ8OQy/sw50EayPYEbh1LfCSWTLGZvjNTeDsjUh46jHD1uHTaBW+WbLkCWuzsIfAjKEZrLkZYeoQ4q4DmPHtw9lQDT6bqMzCkUhEcvi7RY208DwPx7eB3QKszw9m9+GjcDU+T1Tgh/R7JYDDBA74RzFKtpvbScFB5QHTBzDrF+Gf0kp54F8WO7eG3Y5hKWGGwGSNC4TPRRS4kqzEb+k2aeHz28E+pnj4/uJhmeFolHsOthYMXyXwg6UGaeELnALXUhVbwx5XcfDDTP0Og//I1EkLXxxT4Ivn4Qh584JSwJMVeJSp3hz2FgC/T+BLUQWuE/ivm29KCdcReA+up8o3h1kepouDHy9Xygjz98QqHA6MFg8nCbyySwaYn/HmsJPAloLgywS+MVmOJ5LB5JXm4Y9je7AwJQc8TuAz5Xh6642dAvNLMU3gldf/83CDAH8Sq0J6ajee3XlNJvjWqyWGGXeR8O1XdgIcIIXNeBUWp3fh7y+lXIr/YZlh/kjXru0K2eBndyTbbqWCpTvSLwkXdh/Xrt3HssFPb0t2H5cKlvbNkwMWfkK15CdUhRtkVzyR7Pl/OZj/FBYJr+wuOVzIN1aEHy9LCvPfWAWBK7KwtMVNrmpalrxqkgGeJfBqZfrnskKE+ZJ3FS6sMhXhRzdrSgQXV6SL8ENpq38Z4QtcNa4lK/BA2kZIFr6aWAfzp289XFizSYTvLzWWEC68BVmH8wS+QuDf0y1bww5KlzfMN035buy/wPn2j0X41/QxGeFgMLgOLqbjLcI/LxxHmc/nE2C++c/DQvO/YDjb8f5x4V1pYaGVHq/Mwm63ew0uOK7IwfPhGgH+Pj2QJ5wLsUS4aQ3mX5ANsMPheAF+IRLS98Km7QQ1dDQbt2lbhbjNa2xBwHwY46S4mfGJ8LeLuu1hJ4Edhj5QOiXMQ50wDfJ53lFQ6lyep2sVZh62NSLl3Y8PgnuFEEuA1Wq1EBRuhCkBtpv7YdWdhF59Arr+Luj7j8E00A4rwfmw0EHW3CXMvBlB20GkmP24zCnwdcaBsu7ubiHadLmcOViMNk06FYZUSmhUPdCquoQ400hgyyCZNZ9CkiVxEthDYD7aDDsOCtHm3YwPuTC2FyaDATTtgm+UEV5sPowd50csgokYh/g4h8QEhyQZqTgZCQ6TZEwlOUynOMxMhjE3HcD8rBcXP3TiH6arl6CqVE88AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="TabControlTabItemVerticalLeftOffice2K7Black_Normal" backColor="97, 97, 97" foreColor="WhiteSmoke" borderColor="Transparent" backGradientStyle="None" backHatchStyle="None" />
    <resource name="TabControlTabItemVerticalLeftOffice2K7Black_Selected" backColor="Transparent" foreColor="Black" borderColor="Transparent" imageBackgroundStyle="Stretched" backGradientStyle="None" backHatchStyle="None" imageBackgroundStretchMargins="3, 5, 0, 5">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAgAMAAAKJUE5HDQoaCgAAAA1JSERSAAAAFgAAADYIBgAAAMMYaQ0AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAC6UlEQVRYR+3Y21LaUBQG4LyjL+CNow/jjaMP4At45UxBnVKwVtuZVms9gCCBnCCQMxBCUPy71g4ixbF2RnCk7cV/E+DbZGdln6SFhQUsLy9jfX0d6XQauVwO2Wz2t+HvTOb+sw+HR9gpliEtLi6KC4qiwHQ8qF6AvNfG1yDEQbuH92EfqWhAuRN5R9nrDUR26foOZTe6RbqbZK8V47PlQFpdXUWlUkEYhoiiHlzPh+v6cIaxOY4Py/bQsFw0mg7Mho2aacGoW9BrTWhGA4peR0Wto1w1cMn/eHNzE7Ztj2DHdQlxCLBR5x/XGlDpR1W1Brmq47qioySruLpWkC9WcXEl4yxfxulFCSdnRXz7cSUibW1twff9F8CVX+BjQjlSJpMR6HOwwrCiozwOlxL4vCD/CexNwNSHuglFe4CvZU3AhdnAqoAvi/MMfz9/BrYJFjVML0yTatccNqLXqRHxME1UNapbpUZ1yw9TQ2Gs/DiPYFfA7nTgzvzCvSnD7UnYmSrc/Q9n0BrBMQ308wF3ZwR3E7gnYJrrpgUHcwf7s4I9Gny6hPfiGB7N2DwmvypsEKwTrBlDmGZuAdOsMr4c4AjYFXA0gvnte+MwjREMxwIOpgnHI9gXMPfzFGBnEuaSe/NwFM0Atl8FDuYCJnAc9uYA7s8eDoIWwcGMYCq5fxa+eYBbrdHbx7BFS1re5tZpm5vAND3xFlczE1gxxKZSwLwnIZTzl8M1gnl7xv2s8ExNsHwPl9XHsPUE7NzD1vhRQgKLJYBaFzAfJTBcKCkPcCqVgk6TKS8IY6oOAYuBKKmM5JziCZgqQxZrCwPFIcyVwZE2NjaQb1qwCe/QLMLrC14V8Yan3QnRaneosQ411qYybFGDnEAckFh0R01xR27SVaYtnkFZ0SCtrKwgu7+PE6rV4+gGX+IBjvp3jxOPZ4BDyqfeAB+HyUWcWxyGfZy6AcRh09LSEtbW1rC9vQ3u85dkb/8A6UIJPwF/bvjzB8IbBAAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="TabControlTabItemVerticalRightOffice2K7Black_HotTracked" backColor="Transparent" foreColor="WhiteSmoke" imageBackgroundStyle="Stretched" backGradientStyle="None" backHatchStyle="None" imageBackgroundStretchMargins="0, 5, 3, 5">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAARQUAAAKJUE5HDQoaCgAAAA1JSERSAAAAFgAAAFYIBgAAAMvsZlcAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAAErklEQVRYR+3Z2XNTdRQH8PxLOoPOQFuxgFU6lkJa2wJ2gWZ0YIS0TdNmX5vcJLdZb26ztElbilWQVXlhp2lDC/jEMI6OjqOj4wKIgPj69dybdLnQokluCpnx4Td5++TM7/62c47i9KcuHD/qw/RkCFPjHCbTPCZSPMbHeKSFMcojleQxRmM0wSMZ55GIR5GIRRGnEeXC8A+zYBg3TAYDVKpuKJVKKO5k/Zhg3wHn3IGAdSe8xia4dO/BMdACW38rLH1tMPbuhk69FwPqDmjVXehXq2DSqWGz6OEesoH1MQgGhuF2uxAMBtHZ2QnF11knzvK1SLPbEbbvgI9gN8HOgVbYCbZq2mAiWN+zB7qeDug1+2HVHYTD3AOH1ZCH3SIciYTAsj5oNBoovp3V4XyyBp+EtyHt2w7O3gjWIETdAoeWotZQ1ALetxvm/nbYdCo4DQfgsqrzsFUC83wUTqczB18YrcFxbium/HVIuBsQMu+iyJvBUOQumhLxD/rbYB9oh1PfjSHjAbj/Df4+07sEfxyswyjzLjjbTgTMSjFyr64ZrkGac20rHIMEG1bCenhWgT0ejxQ+KoGbpDDNeUHwjzMfivBn3JaCYaeNYFfu44WCy7Df78/DY9U4EZUZ/nlmXx6uLRoepnUswFweDofDUPya2SMDLETMLsHRaLSM8O+Z5hJg3fPhiwSf5Av/eB7n4Nrw3blGXCL4VNGwFX52PeF72fqSYK9bCsdi/DJ8OVUG+H62DlfS1Tg9UviqeDHwg+tbZYHD6wb/OV9bEuxbAQs39hL8cP4NXB2vxplY7VM3yMsKP5rfhGsT1ThbLMxYESCYCw0vwcLuy8ObCN5M8FvywY8XSoUt6w3feF0G2LWO8F8Lr2Fmsgqfx9/Mwe6XHr65ocLgJzdeLR+coVXxRTy/KgqEWc+6w7deqTD47y/LNBUCPDu5EecSBIfoPKbk5n+4QuAnN1cut4qAJVu6cPg/HkJywrc2lAeWXk0CXNiWfgEwXf8zRwhOVAr8eGEjZiYIlnsqBPjaeBW9hPKwXC+hRwRfTVfRw7tYeK33sQCnygHP18gS8SqpQpngh9cpuSkFXjNrEuDxxazpbfngB9ktuEJwLoEsHF6ZmUrgPyRJevHwM0m6AF8m+BQvM3xfKIRUFHxvrqEk+OlikwS+lKrBSbGgJyN8d3YXLpYE2ySwcE6UF/4t01oyvGoJchHOFU1lhH+ZbRersblSerGwtDBdMrxWxVuEf8qoygP/kPloCZ4KEFzALb1Wu6K88HezWpwXGiwRoSVUKLx6S2gZTtbgmNzwN3MW2eFAIJCDz8U20/zWYcxbD97RiKDQwDI2wadvhmcRXrWJ9Rz4qyyDY+FtSDL1CNsawZqacu22QYK0LWKz0KJphVkj9PM6YB3shkPo51nUsFsEeLkDydGdJzQKjUYjFLezEbG1yQ81iK1NhqK0U3Tmvnw78xCNw3uhPfQ+tIc7oOvdB4PmA9hNz7Y2GYZ+g0F0dXVBcWKawZn0fkzHD+JIrA/pEQtS/BDGRjwY5X1I0ohHfRiJMOCC9PijEQnQ2eAfEpuxsZEobY4AvF4PLGYTVCoVlEol/gGzPZeg53OCtQAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="TabControlTabItemVerticalRightOffice2K7Black_Normal" backColor="97, 97, 97" foreColor="WhiteSmoke" borderColor="Transparent" backColorAlpha="Opaque" backGradientStyle="None" backHatchStyle="None" />
    <resource name="TabControlTabItemVerticalRightOffice2K7Black_Selected" foreColor="Black" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="0, 5, 4, 5">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAmAMAAAKJUE5HDQoaCgAAAA1JSERSAAAAFgAAADYIBgAAAMMYaQ0AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsMAAALDAE/QCLIAAADAUlEQVRYR+3Y21LaUBQG4LwjL+CNow/jDaMP4At45UzBOrVgrbYzrdZ6AEEOIeGYM5ATin/X2jGCVetMGxxpe/EPM8B8C5KV7L0iZYtlbO/uYWdn54+yubmJdDqNxcVFpFIpSMemg313hLx/TRnjQxDlI2U/HONA5GaS0cN8pu8c+lc46unI7e5ieXkZUkVWoLZ6aHV0tLs6upqJnm5CMyzopg3Tcih9WHYftjOA0x+iPxhiMHQxdF24rgfP8zD0fOh+gEJPw9raGqRCqQ5OsSyjVGmgUm+iKrdQV9poqB0q2kWzzYU1dERhA5puwbgtajsOFesjDEfwgxAq4ZlMBtL5RQ0chi/u4CbqjV/BJgz6R6Zlw7ZjOITv+9DCK3HM7+BCuS7gy5p6C7cgxzAfKsLFoepFsP4ErD8KX8oCrhBcY1jpQGl2xTngX/2Xw2eEcrgzGC4zXFcFzJ2hNCcnsM0nkHq1Ry3JMHeGxbAzDY/+WZgukhnBdALp6nsBOHzlsOjlaZhuRHMAU8vNHDZmAXu0iswBTJmGzeRgS9wvJnCQJGwjiGFaTF83bNyDPVgTuEpwVcBFhqtKBE/tLRSC1ReHDVO0XEDdwLsim/CZwM7cwf2kYF3AltgQCpgyI9idB5h62ad7BMOD/3AEuwnCtKWdhnngSRamm898wpyEYINeH4EPv1+A8+20hOOzMk6LVZyXeO7jFYXGMy5Eq0pVVmkmaUGhQjyecZEuFdGeg49O5hnmxTWCaTxLGo7nvnKV5usYppW7obYJ7v4Em8/DJ4VKBFOXxLCYr6lTeL5+DrZpxd7Y2ID0lVDOfThqv9+BdV3H+vo6jbylCvUtj7htmvZ5g0I9K0ZcbfIogcZcfkDCzyl0g3qX+pd7mMObQl7zYrhWq2FlZQXSJ83Adj9E1rsWeetfY8sf0+sY20GUN/4NMncZ4507wt4gwBfHRcEaoEGzXocuGFmWkcvlsLCwAGmLfvH7/QPxBiefzz9I/NlT4e9ks1msrq5iaWkJqVQKPwCEAvjzv7JrWAAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
  </resources>
</styleLibrary>