@ECHO OFF
SETLOCAL ENABLEEXTENSIONS
REM https://consultingwerk.atlassian.net/wiki/spaces/SCL/pages/8094574/Scaffolding+utility+for+common+utility+classes

SET scriptpath=%~dp0

if [%1]==[help] goto help

if NOT [%DLC%]==[] goto dlcset 

rem *** Find the most recent version of OpenEdge ***

set KEY_NAME="HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.6\Startup"
set VALUE_NAME=DLC

for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.4\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.4\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.3\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.3\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.2\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.2\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.1\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.1\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.0\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.0\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.7\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.7\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.6\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.6\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.5\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.5\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.4\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.3\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)

:dlcset

echo DLC:       %DLC%

set classname=%1
set suffix=%2

%DLC%\ant\bin\ant -lib %DLC%/PCT/PCT.jar -f %scriptpath%scl-sng.xml generate -Dscriptpath=%scriptpath% -Ddlc=%dlc% -DcurrentFolder=%cd% -Dclassname=%classname% -Dsuffix=%suffix%

goto end

:help

type %scriptpath%\..\..\banner.txt
echo.
echo SmartComponent Library
type %scriptpath%\..\..\frameworkversion.i
echo.
echo.
echo Opening https://documentation.consultingwerkcloud.com/display/SCL/Static+Names+Generator ...

start https://documentation.consultingwerkcloud.com/display/SCL/Static+Names+Generator

:end
