@ECHO OFF
SETLOCAL ENABLEEXTENSIONS
REM https://consultingwerk.atlassian.net/wiki/spaces/SCL/pages/8094029/PASOE+management+scripts

SET scriptpath=%~dp0

if [%1]==[help] goto help

if NOT [%DLC%]==[] goto dlcset 

rem *** Find the most recent version of OpenEdge ***

set KEY_NAME="HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.7\Startup"
set VALUE_NAME=DLC

REM Prefer 32 bit install - in case of 32 bit PDSOE with 64 bit Development Server (which does not include prowin.exe)
REM https://jira.consultingwerkcloud.com/browse/SCL-2190

for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.4\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.4\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.3\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.3\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.2\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.2\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.1\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.1\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.0\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.0\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.7\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.7\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.6\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.6\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.5\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.5\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.4\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.3\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)

:dlcset

if [%1]==[trim]          set action=%1
if [%1]==[query]         set action=%1
if [%1]==[metrics]       set action=%1
if [%1]==[agents]        set action=%1
if [%1]==[sessions]      set action=%1
if [%1]==[clients]       set action=%1
if [%1]==[applist]       set action=%1
if [%1]==[ablobjects]    set action=%1
if [%1]==[agentrequests] set action=%1
if [%1]==[agentsessions] set action=%1
if [%action%]==[]  goto help

if [%2]==[]  goto help

rem throw the first parameter away
shift

echo DLC:       %DLC%
type %DLC%\version
set DRIVE=%scriptpath:~0,2%

rem Go to the script drive 
%DRIVE%
cd %scriptpath%

if exist "%DLC%\bin\prowin.exe" (
%DLC%\bin\prowin -b -p ..\Pasoe\scl-pasoe.p -param "%*" | MORE  
)
if exist %DLC%\bin\prowin32.exe (
%DLC%\bin\prowin32 -b -p ..\Pasoe\scl-pasoe.p -param "%*"  | MORE
)

goto end

:help

type %scriptpath%\..\..\banner.txt
echo.
echo SmartComponent Library
type %scriptpath%\..\..\frameworkversion.i
echo.
echo.
echo Opening https://consultingwerk.atlassian.net/wiki/spaces/SCL/pages/8094029/PASOE+management+scripts ...

start https://consultingwerk.atlassian.net/wiki/spaces/SCL/pages/8094029/PASOE+management+scripts

:end
