@ECHO OFF
SETLOCAL ENABLEEXTENSIONS
REM https://consultingwerk.atlassian.net/wiki/spaces/SCL/pages/713752577/Interface+Generator

SET scriptpath=%~dp0

if [%1]==[help] goto help
if [%1]==[] goto help

if NOT [%ASSEMBLIES%]==[] goto assembliesset 

if exist scl-set-env.cmd                               call scl-set-env.cmd & goto assembliesset   
if exist ..\scl-set-env.cmd                            call ..\scl-set-env.cmd & goto assembliesset   
if exist ..\..\scl-set-env.cmd                         call ..\..\scl-set-env.cmd & goto assembliesset   
if exist ..\..\..\scl-set-env.cmd                      call ..\..\..\scl-set-env.cmd & goto assembliesset   
if exist ..\..\..\..\scl-set-env.cmd                   call ..\..\..\..\scl-set-env.cmd & goto assembliesset   
if exist ..\..\..\..\..\scl-set-env.cmd                call ..\..\..\..\..\scl-set-env.cmd & goto assembliesset   
if exist ..\..\..\..\..\..\scl-set-env.cmd             call ..\..\..\..\..\..\scl-set-env.cmd & goto assembliesset   
if exist ..\..\..\..\..\..\..\scl-set-env.cmd          call ..\..\..\..\..\..\..\scl-set-env.cmd & goto assembliesset   
if exist ..\..\..\..\..\..\..\..\scl-set-env.cmd       call ..\..\..\..\..\..\..\..\scl-set-env.cmd & goto assembliesset   
if exist ..\..\..\..\..\..\..\..\..\scl-set-env.cmd    call ..\..\..\..\..\..\..\..\..\scl-set-env.cmd & goto assembliesset   
if exist ..\..\..\..\..\..\..\..\..\..\scl-set-env.cmd call ..\..\..\..\..\..\..\..\..\..\scl-set-env.cmd & goto assembliesset   

:assembliesset

if NOT [%DLC%]==[] goto dlcset 

rem *** Find the most recent version of OpenEdge ***

set KEY_NAME="HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.7\Startup"
set VALUE_NAME=DLC

for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.4\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.4\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.3\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.3\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.2\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.2\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.1\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.1\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.0\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\12.0\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.7\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.7\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.6\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.6\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.5\Startup" /reg:64 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.5\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.4\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)
for /F "usebackq tokens=3*" %%A IN (`reg query "HKEY_LOCAL_MACHINE\SOFTWARE\PSC\PROGRESS\11.3\Startup" /reg:32 /v "DLC" 2^>nul ^| find "%VALUE_NAME%"`) do ( set DLC=%%A& goto dlcset)

:dlcset

echo DLC:        %DLC%
echo ASSEMBLIES: %ASSEMBLIES%

set className=%1
set interfaceName=%2
set annotation=%3

if exist "%dlc%\ant\bin\ant.cmd" (
  set ant="%dlc%\ant\bin\ant"
) else (
  set ant=ant
)

echo ant:        %ant%

%ant% -lib %scriptpath%../../../Setup/Install/PCT/PCT.jar -f %scriptpath%scl-gen-interface.xml generate -Dscriptpath=%scriptpath% -Ddlc=%dlc% -DcurrentFolder=%cd% -DclassName=%className% -DinterfaceName=%interfaceName% -Dannotation="%annotation%" -Dassemblies="%assemblies%"

goto end

:help

type %scriptpath%\..\..\banner.txt
echo.
echo SmartComponent Library
type %scriptpath%\..\..\frameworkversion.i
echo.
echo.
echo Opening https://consultingwerk.atlassian.net/wiki/spaces/SCL/pages/713752577/Interface+Generator ...

start https://consultingwerk.atlassian.net/wiki/spaces/SCL/pages/713752577/Interface+Generator

:end