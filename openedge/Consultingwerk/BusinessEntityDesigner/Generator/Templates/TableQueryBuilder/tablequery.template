/*------------------------------------------------------------------------
    File        : &1
    Purpose     :
    Syntax      :
    Description :
    Author(s)   : @USER@
    Created     : @DATE@
    Notes       :
  ----------------------------------------------------------------------*/

BLOCK-LEVEL ON ERROR UNDO, THROW.

USING Consultingwerk.OERA.QueryBuilder.* FROM PROPATH .
USING &2.* FROM PROPATH .
USING Progress.Lang.* FROM PROPATH .

@FILEANNOTATION@

CLASS &2.&1
    INHERITS TableQuery:

&4    /**
     * Purpose: Indicates that the following field would be joined using an AND
     * Notes:
     * @return The reference to the &1 instance itself
     */
    DEFINE PUBLIC PROPERTY And AS &1 NO-UNDO
    GET:
        THIS-OBJECT:AndImpl () .

        RETURN THIS-OBJECT .
    END GET .

    /**
     * Purpose: Indicates that the following field would be joined using an AND NOT
     * Notes:
     * @return The reference to the &1 instance itself
     */
    DEFINE PUBLIC PROPERTY AndNot AS &1 NO-UNDO
    GET:
        THIS-OBJECT:AndNotImpl () .

        RETURN THIS-OBJECT .
    END GET .

    /**
     * Purpose: Indicates that the following field would be joined using an OR
     * Notes:
     * @return The reference to the &1 instance itself
     */
    DEFINE PUBLIC PROPERTY Or AS &1 NO-UNDO
    GET:
        THIS-OBJECT:OrImpl () .

        RETURN THIS-OBJECT .
    END GET .

    /**
     * Purpose: Indicates that the following field would be joined using an OR NOT
     * Notes:
     * @return The reference to the &1 instance itself
     */
    DEFINE PUBLIC PROPERTY OrNot AS &1 NO-UNDO
    GET:
        THIS-OBJECT:OrNotImpl () .

        RETURN THIS-OBJECT .
    END GET .

    /**
     * Purpose: Constructor for the &1 class
     * Notes:
     */
    CONSTRUCTOR PUBLIC &1 ():
        SUPER ("&3":U) .

    END CONSTRUCTOR.

&5    /**
     * Purpose: Joins a Query Entry using AND
     * Notes:
     * @param poQuery The &1 instance to join using AND
     * @return The reference to the instance the call was issued on
     */
    METHOD PUBLIC &1 And (poQuery AS &1):

        THIS-OBJECT:AndImpl (poQuery:ToQueryEntry()) .

        RETURN THIS-OBJECT .

    END METHOD.

    /**
     * Purpose: Joins two Query Entries using AND
     * Notes:
     * @param poQuery1 The first &1 instance to join using AND
     * @param poQuery2 The second &1 instance to join using AND
     * @return The reference to the instance the call was issued on
     */
    METHOD PUBLIC &1 And (poQuery1 AS &1,
                                                           poQuery2 AS &1):

        THIS-OBJECT:AndImpl(poQuery1:ToQueryEntry(), poQuery2:ToQueryEntry()) .

        RETURN THIS-OBJECT .

    END METHOD.

    /**
     * Purpose: Joins three Query Entries using AND
     * Notes:
     * @param poQuery1 The first &1 instance to join using AND
     * @param poQuery2 The second &1 instance to join using AND
     * @param poQuery3 The third &1 instance to join using AND
     * @return The reference to the instance the call was issued on
     */
    METHOD PUBLIC &1 And (poQuery1 AS &1,
                                                           poQuery2 AS &1,
                                                           poQuery3 AS &1):

        THIS-OBJECT:AndImpl(poQuery1:ToQueryEntry(), poQuery2:ToQueryEntry(), poQuery3:ToQueryEntry()) .

        RETURN THIS-OBJECT .

    END METHOD.

    /**
     * Purpose: Joins a Query Entry using AND NOT
     * Notes:
     * @param poQuery The &1 instance to join using AND NOT
     * @return The reference to the instance the call was issued on
     */
    METHOD PUBLIC &1 AndNot (poQuery AS &1):

        THIS-OBJECT:AndNotImpl (poQuery:ToQueryEntry()) .

        RETURN THIS-OBJECT .

    END METHOD.

    /**
     * Purpose: Joins two Query Entries using AND NOT
     * Notes:
     * @param poQuery1 The first &1 instance to join using AND NOT
     * @param poQuery2 The second &1 instance to join using AND NOT
     * @return The reference to the instance the call was issued on
     */
    METHOD PUBLIC &1 AndNot (poQuery1 AS &1,
                                                           poQuery2 AS &1):

        THIS-OBJECT:AndNotImpl(poQuery1:ToQueryEntry(), poQuery2:ToQueryEntry()) .

        RETURN THIS-OBJECT .

    END METHOD.

    /**
     * Purpose: Joins three Query Entries using AND NOT
     * Notes:
     * @param poQuery1 The first &1 instance to join using AND NOT
     * @param poQuery2 The second &1 instance to join using AND NOT
     * @param poQuery3 The third &1 instance to join using AND NOT
     * @return The reference to the instance the call was issued on
     */
    METHOD PUBLIC &1 AndNot (poQuery1 AS &1,
                                                           poQuery2 AS &1,
                                                           poQuery3 AS &1):

        THIS-OBJECT:AndNotImpl(poQuery1:ToQueryEntry(), poQuery2:ToQueryEntry(), poQuery3:ToQueryEntry()) .

        RETURN THIS-OBJECT .

    END METHOD.

    /**
     * Purpose: Joins a Query Entry using OR
     * Notes:
     * @param poQuery The &1 instance to join using OR
     * @return The reference to the instance the call was issued on
     */
    METHOD PUBLIC &1 Or (poQuery AS &1):

        THIS-OBJECT:OrImpl(poQuery:ToQueryEntry()) .

        RETURN THIS-OBJECT .

    END METHOD.

    /**
     * Purpose: Joins two Query Entries using OR
     * Notes:
     * @param poQuery1 The first &1 instance to join using OR
     * @param poQuery2 The second &1 instance to join using OR
     * @return The reference to the instance the call was issued on
     */
    METHOD PUBLIC &1 Or (poQuery1 AS &1,
                                                          poQuery2 AS &1):

        THIS-OBJECT:OrImpl(poQuery1:ToQueryEntry(), poQuery2:ToQueryEntry()) .

        RETURN THIS-OBJECT .

    END METHOD.

    /**
     * Purpose: Joins three Query Entries using OR
     * Notes:
     * @param poQuery1 The first &1 instance to join using OR
     * @param poQuery2 The second &1 instance to join using OR
     * @param poQuery3 The third &1 instance to join using OR
     * @return The reference to the instance the call was issued on
     */
    METHOD PUBLIC &1 Or (poQuery1 AS &1,
                                                          poQuery2 AS &1,
                                                          poQuery3 AS &1):

        THIS-OBJECT:OrImpl(poQuery1:ToQueryEntry(), poQuery2:ToQueryEntry(), poQuery3:ToQueryEntry()) .

        RETURN THIS-OBJECT .

    END METHOD.

    /**
     * Purpose: Joins a Query Entry using OR NOT
     * Notes:
     * @param poQuery The &1 instance to join using OR NOT
     * @return The reference to the instance the call was issued on
     */
    METHOD PUBLIC &1 OrNot (poQuery AS &1):

        THIS-OBJECT:OrNotImpl(poQuery:ToQueryEntry()) .

        RETURN THIS-OBJECT .

    END METHOD.

    /**
     * Purpose: Joins two Query Entries using OR NOT
     * Notes:
     * @param poQuery1 The first &1 instance to join using OR NOT
     * @param poQuery2 The second &1 instance to join using OR NOT
     * @return The reference to the instance the call was issued on
     */
    METHOD PUBLIC &1 OrNot (poQuery1 AS &1,
                                                          poQuery2 AS &1):

        THIS-OBJECT:OrNotImpl(poQuery1:ToQueryEntry(), poQuery2:ToQueryEntry()) .

        RETURN THIS-OBJECT .

    END METHOD.

    /**
     * Purpose: Joins three Query Entries using OR NOT
     * Notes:
     * @param poQuery1 The first &1 instance to join using OR NOT
     * @param poQuery2 The second &1 instance to join using OR NOT
     * @param poQuery3 The third &1 instance to join using OR NOT
     * @return The reference to the instance the call was issued on
     */
    METHOD PUBLIC &1 OrNot (poQuery1 AS &1,
                                                          poQuery2 AS &1,
                                                          poQuery3 AS &1):

        THIS-OBJECT:OrNotImpl(poQuery1:ToQueryEntry(), poQuery2:ToQueryEntry(), poQuery3:ToQueryEntry()) .

        RETURN THIS-OBJECT .

    END METHOD.

END CLASS.

