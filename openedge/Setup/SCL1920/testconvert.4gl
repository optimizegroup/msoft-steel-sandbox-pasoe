/* Test if the database contains the iso8859-1 based CLOB's */

DEFINE OUTPUT PARAMETER pcRequireConvert AS CHARACTER NO-UNDO . 

{Consultingwerk/products.i}

FIND {&SmartDB}._file WHERE _file._file-name = "SmartProduct":U NO-LOCK NO-ERROR .

IF NOT AVAILABLE ({&SmartDB}._file) THEN 
	RETURN "0" .

FIND {&SmartDB}._field OF _file WHERE _field._field-name = "ProductDescription":U NO-LOCK . 

IF NOT _field._Charset BEGINS "UTF" THEN
    ASSIGN pcRequireConvert = "true" .

RETURN "0" .    
