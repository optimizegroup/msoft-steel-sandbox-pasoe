/* Conversion routine for ISO8859-1 based CLOB's into UTF-8 */

BLOCK-LEVEL ON ERROR UNDO, THROW .

DEFINE VARIABLE i AS INTEGER     NO-UNDO.

ASSIGN i = 0 .

FOR EACH SmartContextStore EXCLUSIVE-LOCK:
    IF i > 0 AND i MODULO 10 = 0 THEN MESSAGE "[1/7] SmartContextStore.ContextDataset" i  .
    ASSIGN i = i + 1.
    COPY-LOB FROM SmartContextStore.ContextDataset TO SmartContextStore.ContextDataset_UTF8 .
END.

MESSAGE "[1/5] SmartContextStore - Converted" i "records." .

ASSIGN i = 0 .

FOR EACH SmartProduct EXCLUSIVE-LOCK:
    IF i > 0 AND i MODULO 10 = 0 THEN MESSAGE "[2/7] SmartProduct.ProductDescription" i  .
    ASSIGN i = i + 1.
    COPY-LOB FROM SmartProduct.ProductDescription TO SmartProduct.ProductDescription_UTF8 .
END.

MESSAGE "[2/5] SmartProduct - Converted" i "records." .

ASSIGN i = 0 .

FOR EACH SmartSchedulerJob EXCLUSIVE-LOCK:
    IF i > 0 AND i MODULO 10 = 0 THEN MESSAGE "[3/7] SmartSchedulerJob.JobCommand" i .
    ASSIGN i = i + 1.
    COPY-LOB FROM SmartSchedulerJob.JobCommand TO SmartSchedulerJob.JobCommand_UTF8 .
END.

MESSAGE "[3/5] SmartSchedulerJob - Converted" i "records." .

ASSIGN i = 0 .

FOR EACH SmartSchedulerJobPlan EXCLUSIVE-LOCK:
    IF i > 0 AND i MODULO 10 = 0 THEN MESSAGE "[4/7] SmartSchedulerJobPlan.JobParameter" i .
    ASSIGN i = i + 1.
    COPY-LOB FROM SmartSchedulerJobPlan.JobParamter TO SmartSchedulerJobPlan.JobParamter_UTF8 .
    COPY-LOB FROM SmartSchedulerJobPlan.ContextDataSet TO SmartSchedulerJobPlan.ContextDataSet_UTF8 .
END.

MESSAGE "[4/5] SmartSchedulerJobPlan - Converted" i "records." .


ASSIGN i = 0 .

FOR EACH SmartSchedulerJobStatus EXCLUSIVE-LOCK:
    IF i > 0 AND i MODULO 10 = 0 THEN MESSAGE "[6/7] SmartSchedulerJobStatus.JobParamter" i .
    ASSIGN i = i + 1.
    COPY-LOB FROM SmartSchedulerJobStatus.JobParamter TO SmartSchedulerJobStatus.JobParamter_UTF8 .
    COPY-LOB FROM SmartSchedulerJobStatus.ContextDataSet TO SmartSchedulerJobStatus.ContextDataSet_UTF8 .
END.

MESSAGE "[5/5] SmartSchedulerJobStatus - Converted" i "records." .

ASSIGN i = 0 .

RETURN "0" . 
