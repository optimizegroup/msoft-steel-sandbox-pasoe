
/*------------------------------------------------------------------------
    File        : test-old-smart-objectmaster.p
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : MIke Fechner / Consultingwerk Ltd.
    Created     : Tue Mar 30 00:10:10 CEST 2021
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

BLOCK-LEVEL ON ERROR UNDO, THROW.

USING Consultingwerk.SmartFramework.Repository.Object.* FROM PROPATH.

DEFINE VARIABLE cList AS CHARACTER NO-UNDO INITIAL
    "af57f071-b164-ebb8-5514-2e33481317a5,b15f0dcb-6b26-dbb0-5414-b97198c48e09,da087c89-b2a8-61b3-5414-1564b88ddb39,~
efe129bd-7bac-1287-5514-0f33503d0e6e,b15f0dcb-6b26-dbb0-5414-b7716c7c95b9,da087c89-b2a8-61b3-5414-1264987b8e98,~
b15f0dcb-6b26-dbb0-5414-b4716c8ebd7d,d604d26a-0bf4-c987-5a14-9d45544b1208,da087c89-b2a8-61b3-5414-1164785b7188,~
db34d20b-da17-1e82-5414-d7718823157a,b15f0dcb-6b26-dbb0-5414-6e7160fb8c84,da087c89-b2a8-61b3-5414-0d64403651d2,~
efe129bd-7bac-1287-5514-003314dfd261,efe129bd-7bac-1287-5514-173390bc4213":U .

DEFINE VARIABLE i             AS INTEGER                  NO-UNDO.
DEFINE VARIABLE oObjectMaster AS ObjectMasterDatasetModel NO-UNDO.

/* ***************************  Main Block  *************************** */

SESSION:DEBUG-ALERT = TRUE .
SESSION:ERROR-STACK-TRACE = TRUE .

{Consultingwerk/get-service.i Consultingwerk.SmartFramework.ITableInfoProvider
    "NEW Consultingwerk.SmartFramework.System.SmartTableInfoProvider ()"} .

{Consultingwerk/get-service.i Consultingwerk.SmartFramework.IRelationService
    "NEW Consultingwerk.SmartFramework.System.SmartRelationService ()"} .

DO i = 1 TO NUM-ENTRIES (cList):
    IF CAN-FIND (SmartObjectMaster WHERE SmartObjectMaster.ObjectMasterGuid = ENTRY (i, cList)) THEN DO:

        oObjectMaster = NEW ObjectMasterDatasetModel (ENTRY (i, cList)) .

        MESSAGE "Deleting old SmartFramework object:"
                oObjectMaster:SmartObjectMaster:ObjectMasterGuid
                oObjectMaster:SmartObjectMaster:ObjectName .

        oObjectMaster:TrackingChanges = TRUE .
        oObjectMaster:SmartObjectMaster:Delete() .
        oObjectMaster:SaveChanges() .
    END.
END.

RETURN "0":U .
