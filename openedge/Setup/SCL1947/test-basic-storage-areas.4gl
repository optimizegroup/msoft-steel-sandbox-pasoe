/* Test if the database contains the CCS-BSA areas: Data, Index and LOB */

DEFINE OUTPUT PARAMETER pcHasBasicAreas AS CHARACTER NO-UNDO . 

{Consultingwerk/products.i}

IF CAN-FIND (FIRST {&SmartDB}._area WHERE _area._area-name = "Data":U) AND 
   CAN-FIND (FIRST {&SmartDB}._area WHERE _area._area-name = "Index":U) AND
   CAN-FIND (FIRST {&SmartDB}._area WHERE _area._area-name = "LOB":U) 
    THEN ASSIGN pcHasBasicAreas = "true" .

RETURN "0" .    
