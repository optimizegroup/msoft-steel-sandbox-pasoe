/* SCL-2678 : Change GUID of Navigation SmartLinkType */

BLOCK-LEVEL ON ERROR UNDO, THROW.

DO TRANSACTION:
    RUN ConvertNavigationLinks .
END .

RETURN "0":U .

PROCEDURE ConvertNavigationLinks:

    DEFINE BUFFER SmartLinkType      FOR SmartLinkType .
    DEFINE BUFFER SmartLink          FOR SmartLink .
    DEFINE BUFFER SmartModifiedState FOR SmartModifiedState .

    DEFINE VARIABLE iCount     AS INTEGER NO-UNDO.
    DEFINE VARIABLE iRecord    AS INTEGER NO-UNDO.

    DEFINE VARIABLE cContainer AS CHARACTER NO-UNDO.

    FOR EACH SmartLinkType WHERE SmartLinkType.LinkTypeGuid = "eb991bfe-1426-6aad-4b14-bf1124e849cf":U
        EXCLUSIVE-LOCK:

        MESSAGE "Updating SmartLinkType.LinkTypeGuid":U .

        ASSIGN SmartLinkType.LinkTypeGuid = "f52235a4-c120-2490-3814-9e29182ad912":U .
    END.

    FOR EACH SmartSupportedLink WHERE SmartSupportedLink.LinkTypeGuid = "eb991bfe-1426-6aad-4b14-bf1124e849cf":U
        EXCLUSIVE-LOCK:

        MESSAGE "Updating SmartSupportedLink.LinkTypeGuid":U .

        ASSIGN SmartSupportedLink.LinkTypeGuid = "f52235a4-c120-2490-3814-9e29182ad912":U .
    END.    
    
    FOR EACH SmartLink WHERE SmartLink.LinkTypeGuid = "eb991bfe-1426-6aad-4b14-bf1124e849cf":U
        NO-LOCK:

        iCount = iCount + 1 .
    END.

    FOR EACH SmartLink WHERE SmartLink.LinkTypeGuid = "eb991bfe-1426-6aad-4b14-bf1124e849cf":U
        EXCLUSIVE-LOCK:

        ASSIGN iRecord = iRecord + 1 .

        IF CAN-FIND (SmartObjectMaster WHERE SmartObjectMaster.ObjectMasterGuid = SmartLink.ContainerObjectMasterGuid) THEN DO:
            FIND SmartObjectMaster WHERE SmartObjectMaster.ObjectMasterGuid = SmartLink.ContainerObjectMasterGuid NO-LOCK NO-ERROR .

            ASSIGN cContainer = SmartObjectMaster.ObjectName .
        END.
        ELSE
            ASSIGN cContainer = SmartLink.ContainerObjectMasterGuid .

        MESSAGE SUBSTITUTE ("Updating SmartLink.LinkTypeGuid (&1 / &2) in &3":U,
                            iRecord,
                            iCount,
                            cContainer) .

        ASSIGN SmartLink.LinkTypeGuid = "f52235a4-c120-2490-3814-9e29182ad912":U .

        IF NOT CAN-FIND (FIRST SmartModifiedState WHERE SmartModifiedState.ModifiedStateKeyFieldValues = SmartLink.ContainerObjectMasterGuid) THEN DO:
            CREATE SmartModifiedState .
            ASSIGN SmartModifiedState.ModifiedStateKeyFieldValues = SmartLink.ContainerObjectMasterGuid
                   SmartModifiedState.ModifiedStateTable          = "SmartObjectMaster":U
                   SmartModifiedState.ModifiedStateTimeStamp      = NOW
                   SmartModifiedState.ModifiedStateUser           = "setup.xml":U .
        END.
    END.


END PROCEDURE .
