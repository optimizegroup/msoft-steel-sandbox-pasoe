/*------------------------------------------------------------------------
    File        : test-unique-objectname.p
    Purpose     :

    Syntax      :

    Description :

    Author(s)   : Mike Fechner / Consultingwerk Ltd.
    Created     : Fri Dec 27 14:15:49 CET 2019
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

USING Consultingwerk.Util.* FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

{Consultingwerk/products.i}

DEFINE VARIABLE hBuffer    AS HANDLE    NO-UNDO .
DEFINE VARIABLE hBuffer2   AS HANDLE    NO-UNDO .
DEFINE VARIABLE hQuery     AS HANDLE    NO-UNDO .
DEFINE VARIABLE iIndex     AS INTEGER   NO-UNDO .
DEFINE VARIABLE cIndex     AS CHARACTER NO-UNDO .
DEFINE VARIABLE lDuplicate AS LOGICAL   NO-UNDO INITIAL FALSE .

/* ***************************  Main Block  *************************** */

CREATE BUFFER hBuffer FOR TABLE "{&SmartDB}.SmartObjectMaster":U NO-ERROR .

IF NOT VALID-HANDLE (hBuffer) THEN DO:
    MESSAGE "SmartObjectMaster table not yet present in database {&SmartDB}" .

    RETURN "0":U .
END.

CREATE BUFFER hBuffer2 FOR TABLE "{&SmartDB}.SmartObjectMaster":U .

MESSAGE "Processing":U BufferHelper:NumRecords(hBuffer) "records.":U .

ASSIGN hQuery = QueryHelper:CreatePreparedQuery (hBuffer, "preselect each SmartObjectMaster where CustomizationResultGuid = ?":U) .

DO WHILE hBuffer:AVAILABLE TRANSACTION:
    hQuery:GET-CURRENT (EXCLUSIVE-LOCK) .

    MESSAGE "Updating" hBuffer::ObjectName + "." .

    hBuffer::CustomizationResultGuid = "":U .

    hQuery:GET-NEXT (EXCLUSIVE-LOCK) .
END.

REPEAT:
    iIndex = iIndex + 1 .

    ASSIGN cIndex = hBuffer:INDEX-INFORMATION (iIndex) .

    IF cIndex = ? THEN DO:
        MESSAGE "Index ObjectNameCustomization not present. Cannot process." .

        RETURN "1":U .
    END.

    IF ENTRY (1, cIndex) = "ObjectNameCustomization":U THEN DO:
        IF ENTRY (2, cIndex) = "1":U THEN DO:
            MESSAGE "Index ObjectNameCustomization is unique." .

            RETURN "0" .
        END.

        MESSAGE "Index ObjectNameCustomization is NOT unique." .

        LEAVE .
    END.
END .

MESSAGE SKIP
        "Now checking for duplicate ObjectName / CustomizationResultGuid combinations."
        SKIP .

ASSIGN hQuery = QueryHelper:CreatePreparedQuery (hBuffer, "preselect each SmartObjectMaster":U) .

DO WHILE hBuffer:AVAILABLE:
    hBuffer2:FIND-UNIQUE (SUBSTITUTE ("where ObjectName = &1 and CustomizationResultGuid = &2":U,
                                      QUOTER (hBuffer::ObjectName),
                                      QUOTER (hBuffer::CustomizationResultGuid))) NO-ERROR .

    IF NOT hBuffer2:AVAILABLE THEN DO:
        MESSAGE SUBSTITUTE ("Duplicate found for ObjectName &1 CustomizationResultGuid &2.",
                            QUOTER (hBuffer::ObjectName),
                            QUOTER (hBuffer::CustomizationResultGuid)) .
        lDuplicate = TRUE .
    END.

    hQuery:GET-NEXT (NO-LOCK) .
END.

IF lDuplicate THEN DO:
    MESSAGE SKIP
            "Duplicate SmartObjectMaster records for ObjectName / CustomizationResultGuid found. Please correct before upgrading SmartDB schema.":U .

    RETURN "1":U .
END.
ELSE
    RETURN "0":U .
